//============================================================================
// Name        : RadEngine.cpp
// Author      : Steffen Kremer
// Version     :
// Copyright   : Games-Table.Studio
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "RadEngine/RadEngine.hpp"
#include "RadEngine/Game/State/EngineStartup.hpp"

using namespace rad::engine;

int main(int argc, char *argv[])
{
	if (Engine->Init(argc, argv))
	{
		Debug->SetLevel(true,true);
		Engine->ChangeState(CEngineStartupState::Instance());
		//Engine->ChangeState(CLoadingState::Instance());
		while(Engine->IsRunning())
		{
			Engine->HandleEvents();
			Engine->Update();
			Engine->Draw();
		}
		Engine->Destroy();
	}
	return 0;
}
