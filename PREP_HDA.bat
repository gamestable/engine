@echo off
cd .\Data\
echo Transfering LoadingScreen.png...
bin2h -c DataTextureLoadingScreen < ..\LoadingScreen.png > DataTextureLoadingScreen.hpp
echo const char DataTextureLoadingScreen_Name[] = "LoadingScreen.png"; >> DataTextureLoadingScreen.hpp
echo Transfering OpenSans-Regular.ttf...
Fbin2h -c DataFontStandard < ..\OpenSans-Regular.ttf > DataFontStandard.hpp
Fecho const char DataFontStandard_Name[] = "OpenSans-Regular.ttf"; >> DataFontStandard.hpp
echo !!!
echo WARNING: replace 0x with (char)0x!!!
echo !!!
PAUSE