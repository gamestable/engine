const char DataShaderFreeType2Vertex[]={"attribute vec3 vertPos;attribute vec2 vertTexcoord0;uniform mat4 matProjViewModel;uniform mat4 matTex;varying vec2 texcoord;void main(){texcoord = (matTex*vec4(vertTexcoord0, 1.0, 1.0)).xy;gl_Position = matProjViewModel*vec4(vertPos, 1.0);}" };

const char DataShaderFreeType2Fragment[]={ "uniform lowp sampler2D mTexture0;varying vec2 texcoord;void main(){gl_FragColor = texture2D(mTexture0, texcoord);}" };

const char DataShaderFreeType2_Name[] = "freetype2x";
