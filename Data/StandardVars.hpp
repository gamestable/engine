/*
 * StandardVars.hpp
 *
 *  Created on: Oct 13, 2016
 *      Author: Steffen Kremer
 */

#ifndef DATA_STANDARDVARS_HPP_
#define DATA_STANDARDVARS_HPP_

addVarBool("debug_error_resource_loading_abortonerror",false,false);

#endif /* DATA_STANDARDVARS_HPP_ */
