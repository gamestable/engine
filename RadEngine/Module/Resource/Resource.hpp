/*
 * ResourceModule.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_RESOURCE_RESOURCE_HPP_
#define RADENGINE_MODULE_RESOURCE_RESOURCE_HPP_

#include "RadEngine/Datatypes/Index.hpp"
#include "RadResourceLibrary.hpp"
#include <vector>

namespace rad
{
namespace engine
{

class CResourceLoader {
public:
	~CResourceLoader();

	bool OpenContainer(std::string file);
	bool LoadResources(Index container);
	bool LoadStandardResources();

	resourcelibrary::CResource LoadResource(Index container, Index resource, bool loadData);

	Index GetContainerID(std::string containerName);
	Index GetResourceID(Index containerID, std::string resourceName);
	Index GetContainerCount() {return this->m_containers.size();};

private:
	bool m_standardResourcesLoaded = false;
	std::vector<resourcelibrary::CContainer *> m_containers;
};

}
}

#endif /* RADENGINE_MODULE_RESOURCE_RESOURCE_HPP_ */
