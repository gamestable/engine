/*
 * CResourceLoader.cpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#include "RadEngine/Module/Resource/Resource.hpp"
#include "../../RadEngine.hpp"
#include "../File/File.hpp"

//Data Headers
#include "Data/DataFontStandard.hpp"
#include "Data/DataTextureLoadingScreen.hpp"
#include "Data/DataShaderFreeType2.hpp"

namespace rad
{
namespace engine
{

CResourceLoader::~CResourceLoader()
{
	if (m_containers.size())
	{
		for (unsigned int i = 0;i < m_containers.size();++i)
		{
			//RadEngine.debug.log("Releasing Container: " + m_containers[i]->getName());
			if (m_containers.at(i)!=nullptr)
			{
				RadEngine.Debug.Log("Released Container: " + m_containers[i]->GetName());
				delete m_containers[i];
				m_containers[i] = nullptr;
			}
		}
		m_containers.clear();
	}
}

bool CResourceLoader::OpenContainer(std::string file)
{
	resourcelibrary::CContainer *tmp = new resourcelibrary::CContainer;
	//tmp->setSTDOUT(true);
	if (!tmp->Open(file))
	{
		delete tmp;
		return false;
	}
	if (!tmp->IsOpen())
	{
		delete tmp;
		return false;
	}
	RadEngine.Debug.Log("Loaded Container: "+file);
	m_containers.push_back(tmp);
	return true;
}

bool CResourceLoader::LoadResources(Index container)
{
	if (container-1<m_containers.size())
	{
		if (m_containers.at(container-1)==nullptr)
		{
			RadEngine.Debug.Warning("Container nullptr! "+std::to_string(container)+"(-1)");
			return false;
		}
		RadEngine.Debug.Log("Container: "+m_containers.at(container-1)->GetName()+" File Count: "+std::to_string(m_containers.at(container-1)->GetFileCount()));
		for(unsigned int i=1; i<=m_containers.at(container-1)->GetFileCount(); ++i)
		{
			//RadEngine.debug.log("File: "+std::to_string(i));
			rad::resourcelibrary::CResource res;
			m_containers.at(container-1)->Load(i,&res,true);
			switch(res.m_type)
			{
			case 1: //Texture
				{
					CResourceTexture* x = new CResourceTexture();
					if (!x->LoadFromImageData(res.m_name,res.m_data,res.m_uncompressedSize))
					{
						RadEngine.Debug.Log("FAILED loading Texture: "+res.m_name);
						/*if (RadEngine.var.getVarBool("debug_error_resource_loading_abortonerror"))
						{
							RadEngine.debug.log("Abort...");
							return false;
						}*/
					}
					RadEngine.Debug.Log("Loaded Texture: "+res.m_name);
				}
				break;
			case 3:
				{
					//CResourceSound * x = new CResourceSound();
					/*if (!x->LoadFromMemory(res.m_name,res.m_data,res.m_uncompressedSize))
					{
						RadEngine.Debug.Log("FAILED loading Sound: "+res.m_name);
						if (RadEngine.var.getVarBool("debug_error_resource_loading_abortonerror"))
						{
							RadEngine.Debug.Log("Abort...");
							return false;
						}
					}*/
				}
				break;
			case 7: //Vertex Shader
				{
					/*std::string vertShadStr((char*)res.data,res.uncompressedSize);
					RadResourceLibrary::RadResource fragShadRes;
					RadResourceLibrary::RadResourceContainer *conTmp = m_containers.at(container-1);
					unsigned int fragShadId = conTmp->findIDbyName(FileName::removeExtension(res.name)+".frag");
					conTmp->load(fragShadId,&fragShadRes,true);
					if (fragShadRes.data!=nullptr)
					{
						std::string fragShadStr((char*)fragShadRes.data,fragShadRes.uncompressedSize);
						ShaderProgram *tmp = new ShaderProgram;
						if (!tmp->loadProgram(FileName::fileNameToName(res.name),vertShadStr.c_str(),fragShadStr.c_str()))
						{
							RadEngine.debug.log("FAILED loading Shader: "+res.name+"/"+fragShadRes.name);
							if (RadEngine.var.getVarBool("debug_error_resource_loading_abortonerror"))
							{
								RadEngine.debug.log("Abort...");
								return false;
							}
						}
						RadEngine.debug.log("Loaded Shader: "+res.name+"/"+fragShadRes.name);
					}
					else
					{
						RadEngine.debug.error("Cant find Fragment Shader for: "+res.name);
						if (RadEngine.var.getVarBool("debug_error_resource_loading_abortonerror"))
						{
							RadEngine.debug.log("Abort...");
							return false;
						}
					}*/
				}
				break;
			case 8: //Fragment Shader
				{
					//Loaded by vertex shader file above
					RadEngine.Debug.Log("Skipping "+res.m_name+", because Fragment Shader...");
				}
				break;
			case 9: //TrueTypeFont
				{
					/*for (unsigned int o=10; o<=45; o+=1)
					{
						if (!RadEngine.resource.font.loadFont(res.name,res.data,res.uncompressedSize,o))
						{
							RadEngine.debug.log("FAILED loading TrueType Font: "+res.name+" with size: "+std::to_string(o));
							if (RadEngine.var.getVarBool("debug_error_resource_loading_abortonerror"))
							{
								RadEngine.debug.log("Abort...");
								return false;
							}
						}
					}*/
					RadEngine.Debug.Log("Loaded TrueType Font: "+res.m_name+" Size: 10-75");
				}
				break;
			}
		}
	}
	return true;
}

bool CResourceLoader::LoadStandardResources()
{
	if (!m_standardResourcesLoaded)
	{
		m_standardResourcesLoaded = true;
		/*ShaderProgram * tmpp = new ShaderProgram;
		if (!tmpp->LoadProgram(std::string(DataShaderFreeType2_Name),DataShaderFreeType2Vertex,DataShaderFreeType2Fragment))
		{
			RadEngine.Debug.Warning("Cant load Standard FreeType Shader!");
		}
		ResourceTexture * tmp = new ResourceTexture;
		bool success = tmp->LoadFromImageData(std::string(DataTextureLoadingScreen_Name), (void *)(DataTextureLoadingScreen), DataTextureLoadingScreen_size);
		if (success==false)
		{
			RadEngine.Debug.Warning("Cant load LoadingScreen Texture!");
		}
		for (unsigned int o=10; o<=45&&success; o+=1)
		{
			success = success&&RadEngine.Resource.Font.LoadFont(std::string(DataFontStandard_Name), (void *)DataFontStandard,DataFontStandard_size,o);
		}
		if (success==false)
		{
			RadEngine.Debug.Warning("Cant load Standard font!");
		}*/
		return true;
	}
	return false;
}

resourcelibrary::CResource CResourceLoader::LoadResource(Index container, Index resource, bool loadData)
{
	resourcelibrary::CResource tmp;
	if (container-1>=0&&container<=m_containers.size())
	{
		m_containers[container-1]->Load(resource,&tmp,loadData);
	}
	return tmp;
}

Index CResourceLoader::GetContainerID(std::string name)
{
	if (m_containers.size())
	{
		for (unsigned int i = 0; i < m_containers.size(); ++i)
		{
			if (m_containers[i]->GetName()==name)
			{
				return i+1;
			}
		}
	}
	return 0;
}

Index CResourceLoader::GetResourceID(Index containerID, std::string resourceName)
{
	if (containerID-1>=0&&containerID<=m_containers.size())
	{
		return m_containers[containerID-1]->FindIDbyName(resourceName);
	}
	return 0;
}

}
}
