/*
 * Display.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_OS_DISPLAY_HPP_
#define RADENGINE_MODULE_OS_DISPLAY_HPP_


namespace rad
{
namespace engine
{

class CScreen
{
public:
	static unsigned int GetSizeX();
	static unsigned int GetSizeY();

	static unsigned char GetMonitorCount();
};

}
}

#endif /* RADENGINE_MODULE_OS_DISPLAY_HPP_ */
