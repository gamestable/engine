/*
 * Display.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#include "Display.hpp"
#include "windows.h"

namespace rad
{
namespace engine
{

unsigned int CScreen::GetSizeX()
{
	return static_cast<unsigned int>(GetSystemMetrics(SM_CXSCREEN));
}

unsigned int CScreen::GetSizeY()
{
	return static_cast<unsigned int>(GetSystemMetrics(SM_CYSCREEN));
}

unsigned char CScreen::GetMonitorCount()
{
	return static_cast<unsigned char>(GetSystemMetrics(SM_CMONITORS));
}

}
}
