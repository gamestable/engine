/*
 * OS.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_OS_OS_HPP_
#define RADENGINE_MODULE_OS_OS_HPP_

#include "Display.hpp"

namespace rad
{
namespace engine
{

class COS
{
public:
	CScreen Screen;
};

}
}

#endif /* RADENGINE_MODULE_OS_OS_HPP_ */
