/*
 * Timer.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_TIME_TIMER_HPP_
#define RADENGINE_MODULE_TIME_TIMER_HPP_


namespace rad
{
namespace engine
{

class CTimer
{
public:
	//Initializes variables
	CTimer();

	//The various clock actions
	void Start();
	void Stop();
	void Pause();
	void Unpause();

	//Gets the timer's time
	unsigned int GetTicks();

	//Checks the status of the timer
	bool IsStarted();
	bool IsPaused();

private:
	//The clock time when the timer started
	unsigned int m_startTicks = 0;

	//The ticks stored when the timer was paused
	unsigned int m_pausedTicks = 0;

	//The timer status
	bool m_paused = false;
	bool m_started = false;
};

}
}

#endif /* RADENGINE_MODULE_TIME_TIMER_HPP_ */
