/*
 * Time.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_TIME_TIME_HPP_
#define RADENGINE_MODULE_TIME_TIME_HPP_

#include "RadEngine/Extern/includeSDL.hpp"

namespace rad
{
namespace engine
{

class CTime
{
public:
	int SinceStart() {return SDL_GetTicks();};
	inline double Delta() {return this->m_deltaTime;};
	inline double ScaledDelta() {return this->m_deltaTime * timeScale;};

	void Update();
	/*void update() {this->_deltaTimeNewTime = SDL_GetTicks();
	this->_deltaTime = this->_deltaTimeNewTime - this->_deltaTimeOldTime;
	this->_deltaTimeOldTime = this->_deltaTimeNewTime;};*/

	float timeScale = 1.0f;

private:
	float m_deltaTimeOldTime = 0;
	float m_deltaTimeNewTime = SDL_GetPerformanceCounter();
	double m_deltaTime = 0;
};

}
}

#endif /* RADENGINE_MODULE_TIME_TIME_HPP_ */
