/*
 * Time.cpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#include "Time.hpp"

namespace rad
{
namespace engine
{

void CTime::Update()
{
	this->m_deltaTimeOldTime = this->m_deltaTimeNewTime;
	this->m_deltaTimeNewTime = SDL_GetPerformanceCounter();
	/*double t = (double)((this->m_deltaTimeNewTime-this->m_deltaTimeOldTime)*1000/SDL_GetPerformanceFrequency())*0.05f;
	if (t<2.0f)
	{
		this->m_deltaTime = t * this->timeScale;
	}*/
	this->m_deltaTime = (double)(this->m_deltaTimeNewTime-this->m_deltaTimeOldTime);
	/*if (engine.display.fps._current==0)
	{
		this->_deltaTime = 0.f;
	}
	else
	{
		this->_deltaTime = engine.display.fps._target/engine.display.fps._current;
	}*/
}

}
}
