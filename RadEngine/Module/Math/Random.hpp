/*
 * Random.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_MATH_RANDOM_HPP_
#define RADENGINE_MODULE_MATH_RANDOM_HPP_


namespace rad
{
namespace engine
{

class CRandom
{
public:
	float Range(float floor, float ceiling);
};

}
}

#endif /* RADENGINE_MODULE_MATH_RANDOM_HPP_ */
