/*
 * Collision.cpp
 *
 *  Created on: Feb 7, 2017
 *      Author: Steffen Kremer
 */

#include "Collision.hpp"

namespace rad
{
namespace engine
{

bool CCollision::PositionInRect(float x, float y, float rx, float ry, float rw, float rh)
{
	if (x>=rx&&x<=rx+rw)
	{
		if (y>=ry&&y<=ry+rh)
		{
			return true;
		}
	}
	return false;
}

}
}
