/*
 * Math.cpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#include "Math.hpp"

namespace rad
{
namespace engine
{

float CMath::LengthDirX(float length, float rad)
{
	return cos(rad) * length;
}

float CMath::LengthDirY(float length, float rad)
{
	return sin(rad) * length;
}

unsigned int CMath::PowerOfTwo(unsigned int num)
{
	if (num!=0)
	{
		num--;
		num |= (num >> 1);
		num |= (num >> 2);
		num |= (num >> 4);
		num |= (num >> 8);
		num |= (num >> 16);
		num++;
	}
	return num;
}

}
}
