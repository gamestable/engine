/*
 * Random.cpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#include "Random.hpp"
#include <cstdlib>

namespace rad
{
namespace engine
{

float CRandom::Range(float floor, float ceiling)
{
	float range = ceiling-floor;
	return floor + float((range * rand()) / (RAND_MAX + 1.0));
}

}
}
