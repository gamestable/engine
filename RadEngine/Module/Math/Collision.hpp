/*
 * Collision.hpp
 *
 *  Created on: Feb 7, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_MATH_COLLISION_HPP_
#define RADENGINE_MODULE_MATH_COLLISION_HPP_

#include "RadEngine/Datatypes/Vector2.hpp"
#include "RadEngine/Datatypes/Rect.hpp"

namespace rad
{
namespace engine
{

class CCollision
{
public:
	template <typename T>
	static bool PositionInRect(Vector2<T>, Rect<T>);
	template <typename T>
	static bool PositionInRect(float x, float y, Rect<T>);
	static bool PositionInRect(float x, float y, float rx, float ry, float rw, float rh);
};

}
}

#endif /* RADENGINE_MODULE_MATH_COLLISION_HPP_ */
