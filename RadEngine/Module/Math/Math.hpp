/*
 * Math.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_MATH_MATH_HPP_
#define RADENGINE_MODULE_MATH_MATH_HPP_

#include "Random.hpp"
#include "Hash.hpp"
#include "Collision.hpp"

namespace rad
{
namespace engine
{

class CMath
{
public:
	static float LengthDirX(float length, float rad);
	static float LengthDirY(float length, float rad);
	static unsigned int PowerOfTwo(unsigned int num);
	static float Rad2Deg(float rad)
	{
		return (180.0f / pi) * rad;
	}
	static float Deg2Rad(float deg)
	{
		return (pi / 180.0f) * deg;
	}

	static constexpr float pi = 3.14159265358979323846f;

	CRandom Random;
	CHash Hash;
	CCollision Collision;
};

}
}

#endif /* RADENGINE_MODULE_MATH_MATH_HPP_ */
