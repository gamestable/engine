/*
 * Input.cpp
 *
 *  Created on: Jan 20, 2017
 *      Author: Steffen Kremer
 */

#include "Input.hpp"
#include "SDLToRad.hpp"
#include "RadEngine/RadEngine.hpp"
#include <string.h>

namespace rad
{
namespace engine
{

CInput::CKeyboard::CKeyboard()
{
	memset(m_array, 0, sizeof(CInput::State)*KeyboardCode::KEYBOARD_CODE_SIZE);
}

void CInput::CKeyboard::m_Update()
{
	for (unsigned int i=0;i<KeyboardCode::KEYBOARD_CODE_SIZE;i++)
	{
		m_array[i].pressed = false;
		m_array[i].released = false;
		m_array[i].doubleClick = false;
	}
}

void CInput::CKeyboard::m_EventUpdate(SDL_Event event)
{
	if (event.type==SDL_KEYDOWN||event.type==SDL_KEYUP)
	{
		if (event.key.keysym.sym<KeyboardCode::KEYBOARD_CODE_SIZE)
		{
			//RadEngine.Debug.Log(std::to_string(SDLToRadKeyCode(event.key.keysym.sym)));
			if (event.type==SDL_KEYDOWN)
			{
				m_array[SDLToRadKeyCode(event.key.keysym.sym)].pressed = true;
				m_array[SDLToRadKeyCode(event.key.keysym.sym)].down = true;
			}
			else if (event.type==SDL_KEYUP)
			{
				m_array[SDLToRadKeyCode(event.key.keysym.sym)].released = true;
				m_array[SDLToRadKeyCode(event.key.keysym.sym)].down = false;
			}
		}
	}
}

//#############################

CInput::CMouse::CMouse()
{
	memset(m_array, 0, sizeof(CInput::State)*MouseCode::MOUSE_CODE_SIZE);
}

void CInput::CMouse::m_Update()
{
	for (unsigned int i=0;i<MouseCode::MOUSE_CODE_SIZE;i++)
	{
		//this->_lastState[i] = this->_buttons[i].down;
		m_array[i].pressed = false;
		m_array[i].released = false;
		m_array[i].doubleClick = false;
	}
	this->m_zV = 0;
	this->m_xV = 0;
	this->m_yV = 0;
}

void CInput::CMouse::m_EventUpdate(SDL_Event event)
{
	if (event.type==SDL_MOUSEMOTION)
	{
		float mX = (float)event.motion.x-(float)RadEngine.Display.native.offsetX;
		float mY = (float)event.motion.y-(float)RadEngine.Display.native.offsetY;
		if (mX<0) mX = 0;
		if (mY<0) mY = 0;
			m_xR = (mX/((float)RadEngine.Display.native.sizeX-(float)RadEngine.Display.native.offsetX*2))*(float)RadEngine.Display.screen.sizeX;
		if (m_xR>(int)RadEngine.Display.screen.sizeX) m_xR = RadEngine.Display.screen.sizeX;
			m_yR = (mY/((float)RadEngine.Display.native.sizeY-(float)RadEngine.Display.native.offsetY*2))*(float)RadEngine.Display.screen.sizeY;
		if (m_yR>(int)RadEngine.Display.screen.sizeY) m_yR = RadEngine.Display.screen.sizeY;
			m_xV = ((float)event.motion.xrel/(float)RadEngine.Display.native.sizeX)*(float)RadEngine.Display.screen.sizeX;
		m_yV = ((float)event.motion.yrel/(float)RadEngine.Display.native.sizeX)*(float)RadEngine.Display.screen.sizeX;
		m_xA = m_xR + RadEngine.Display.Camera.GetX();
		m_yA = m_yR + RadEngine.Display.Camera.GetY();
	}
	else if (event.type==SDL_MOUSEBUTTONDOWN || event.type==SDL_MOUSEBUTTONUP)
	{
		State tmp;
		if (event.button.state==SDL_PRESSED)
		{
			tmp.pressed = true;
			tmp.down = true;
		}
		else if (event.button.state==SDL_RELEASED)
		{
			tmp.released = true;
			tmp.down = false;
		}
		if (event.button.clicks==2)
			tmp.doubleClick = true;
		else
			tmp.doubleClick = false;
		m_array[event.button.button+1] = tmp;
	}
	else if (event.type==SDL_MOUSEWHEEL)
	{
		m_zV = event.wheel.y*2;
	}
}

}
}
