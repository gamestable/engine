/*
 * KeyboardCodes.hpp
 *
 *  Created on: Jan 20, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_INPUT_KEYBOARDCODES_HPP_
#define RADENGINE_MODULE_INPUT_KEYBOARDCODES_HPP_


namespace rad
{
namespace engine
{

typedef enum
{
	KEYBOARD_UNKNOWN = 0,
	KEYBOARD_ANY = 1,
	//Punctuation Mark
	KEYBOARD_EXLAIM = '!',
	KEYBOARD_DBLQUOTE = '"',
	KEYBOARD_HASH = '#',
	KEYBOARD_PERCENT = '%',
	KEYBOARD_DOLLAR = '$',
	KEYBOARD_AND = '&',
	KEYBOARD_QUOTE = '\'',
	KEYBOARD_LEFTPAREN = '(',
	KEYBOARD_RIGHTPAREN = ')',
	KEYBOARD_ASTERISK = '*',
	KEYBOARD_PLUS = '+',
	KEYBOARD_COMMA = ',',
	KEYBOARD_MINUS = '-',
	KEYBOARD_PERIOD = '.',
	KEYBOARD_SLASH = '/',
	KEYBOARD_COLON = ':',
	KEYBOARD_SEMICOLON = ';',
	KEYBOARD_LESS = '<',
	KEYBOARD_EQUALS = '=',
	KEYBOARD_GREATER = '>',
	KEYBOARD_QUESTION = '?',
	KEYBOARD_AT = '@',
	KEYBOARD_LEFTBRACKET = '[',
	KEYBOARD_BACKSLASH = '\\',
	KEYBOARD_RIGHTBRACKET = ']',
	KEYBOARD_CARET = '^',
	KEYBOARD_UNDERSCORE = '_',
	KEYBOARD_BACKQUOTE = '`',
	//Alphabet
	KEYBOARD_A = 'a',
	KEYBOARD_B = 'b',
	KEYBOARD_C = 'c',
	KEYBOARD_D = 'd',
	KEYBOARD_E = 'e',
	KEYBOARD_F = 'f',
	KEYBOARD_G = 'g',
	KEYBOARD_H = 'h',
	KEYBOARD_I = 'i',
	KEYBOARD_J = 'j',
	KEYBOARD_K = 'k',
	KEYBOARD_L = 'l',
	KEYBOARD_M = 'm',
	KEYBOARD_N = 'n',
	KEYBOARD_O = 'o',
	KEYBOARD_P = 'p',
	KEYBOARD_Q = 'q',
	KEYBOARD_R = 'r',
	KEYBOARD_S = 's',
	KEYBOARD_T = 't',
	KEYBOARD_U = 'u',
	KEYBOARD_V = 'v',
	KEYBOARD_W = 'w',
	KEYBOARD_X = 'x',
	KEYBOARD_Y = 'y',
	KEYBOARD_Z = 'z',
	//Numeric
	KEYBOARD_0 = '0',
	KEYBOARD_1 = '1',
	KEYBOARD_2 = '2',
	KEYBOARD_3 = '3',
	KEYBOARD_4 = '4',
	KEYBOARD_5 = '5',
	KEYBOARD_6 = '6',
	KEYBOARD_7 = '7',
	KEYBOARD_8 = '8',
	KEYBOARD_9 = '9',
	//Function
	KEYBOARD_F1,
	KEYBOARD_F2,
	KEYBOARD_F3,
	KEYBOARD_F4,
	KEYBOARD_F5,
	KEYBOARD_F6,
	KEYBOARD_F7,
	KEYBOARD_F8,
	KEYBOARD_F9,
	KEYBOARD_F10,
	KEYBOARD_F11,
	KEYBOARD_F12,
	//Other Function
	KEYBOARD_ESC = '\033',
	KEYBOARD_TAB = '\t',
	KEYBOARD_CAP,
	KEYBOARD_SHIFT_LEFT,
	KEYBOARD_CTRL_LEFT,
	KEYBOARD_OS_LEFT,
	KEYBOARD_ALT,
	KEYBOARD_SPACE = ' ',
	KEYBOARD_ALT_GR,
	KEYBOARD_OS_RIGHT,
	KEYBOARD_MENU,
	KEYBOARD_CTRL_RIGHT,
	KEYBOARD_SHIFT_RIGHT,
	KEYBOARD_RETURN = '\r',
	KEYBOARD_BACKSPACE = '\b',
	//Other Function Right
	KEYBOARD_PRINT,
	KEYBOARD_ROLE,
	KEYBOARD_BREAK,
	KEYBOARD_INSERT,
	KEYBOARD_DELETE = '\177',
	KEYBOARD_HOME,
	KEYBOARD_END,
	KEYBOARD_PAGE_UP,
	KEYBOARD_PAGE_DOWN,
	//Arrow Keys
	KEYBOARD_LEFT,
	KEYBOARD_RIGHT,
	KEYBOARD_UP,
	KEYBOARD_DOWN,
	//Numpad
	//KEYBOARD_PAD_,
	//Engine
	KEYBOARD_CODE_LASTPRESSED = 510,
	KEYBOARD_CODE_LASTRELEASED = 511,
	KEYBOARD_CODE_SIZE = 512
} KeyboardCode;

typedef enum
{
	MOUSE_UNKNOWN = 0,
	MOUSE_ANY = 1,
	MOUSE_LEFT = 2,
	MOUSE_MIDDLE,
	MOUSE_RIGHT,
	MOUSE_X1,
	MOUSE_X2,
	//Engine
	MOUSE_CODE_LASTPRESSED = 7,
	MOUSE_CODE_LASTRELEASED,
	MOUSE_CODE_SIZE = 9
} MouseCode;

}
}

#endif /* RADENGINE_MODULE_INPUT_KEYBOARDCODES_HPP_ */
