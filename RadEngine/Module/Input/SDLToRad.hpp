/*
 * SDLToRad.hpp
 *
 *  Created on: Jan 28, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_INPUT_SDLTORAD_HPP_
#define RADENGINE_MODULE_INPUT_SDLTORAD_HPP_

#include "KeyboardCodes.hpp"
#include "RadEngine/Extern/IncludeSDL.hpp"

namespace rad
{
namespace engine
{

inline KeyboardCode SDLToRadKeyCode(SDL_Keycode code)
{
	switch(code)
	{
	case SDLK_UNKNOWN:
		return KEYBOARD_UNKNOWN;
	case SDLK_RETURN:
		return KEYBOARD_RETURN;
	case SDLK_ESCAPE:
		return KEYBOARD_ESC;
	case SDLK_BACKSPACE:
		return KEYBOARD_BACKSPACE;
	case SDLK_TAB:
		return KEYBOARD_TAB;
	case SDLK_SPACE:
		return KEYBOARD_SPACE;
	case SDLK_EXCLAIM:
		return KEYBOARD_EXLAIM;
	default:
		return static_cast<KeyboardCode>(code);
	}
}

}
}

#endif /* RADENGINE_MODULE_INPUT_SDLTORAD_HPP_ */
