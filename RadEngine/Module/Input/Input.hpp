/*
 * Input.hpp
 *
 *  Created on: Jan 20, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_INPUT_INPUT_HPP_
#define RADENGINE_MODULE_INPUT_INPUT_HPP_

#include <iostream>
#include "KeyboardCodes.hpp"
#include "../../Extern/IncludeSDL.hpp"

namespace rad
{
namespace engine
{

class CInput
{
public:
	struct State
	{
		bool pressed = false;
		bool released = false;
		bool down = false;
		bool doubleClick = false;
	};
	class CAxes
	{
	public:
		class CAxe
		{
		public:
			std::string name = "";
		};
	} Axes;
	class CKeyboard
	{
	public:
		CKeyboard();

		bool Pressed(KeyboardCode key) {if (key<KeyboardCode::KEYBOARD_CODE_SIZE) return m_array[key].pressed; else return false;}
		bool Released(KeyboardCode key) {if (key<KeyboardCode::KEYBOARD_CODE_SIZE) return m_array[key].released; else return false;}
		bool Down(KeyboardCode key) {if (key<KeyboardCode::KEYBOARD_CODE_SIZE) return m_array[key].down; else return false;}

		KeyboardCode LastPressed();
		KeyboardCode LastReleased();
		KeyboardCode LastDown();

		void m_Update();
		void m_EventUpdate(SDL_Event);

	private:
		State m_array[KeyboardCode::KEYBOARD_CODE_SIZE];
	} Keyboard;
	class CMouse
	{
	public:
		CMouse();

		bool Pressed(MouseCode button);
		bool Released(MouseCode button);
		bool Down(MouseCode button);

		State Last(MouseCode button);

		int GetX() {return m_xR;}
		int GetY() {return m_yR;};
		int GetAbsX() {return m_xA;};
		int GetAbsY() {return m_yA;};
		int GetVelX() {return m_xV;};
		int GetVelY() {return m_yV;};
		int GetVelZ() {return m_zV;};

		void m_Update();
		void m_EventUpdate(SDL_Event);

	private:
		State m_array[MouseCode::MOUSE_CODE_SIZE];
		int m_xR = 0; //rel pos on screen
		int m_yR = 0;
		int m_xA = 0; //Absolute pos in room
		int m_yA = 0;
		int m_xV = 0; //Velocity
		int m_yV = 0;
		int m_zV = 0; //Wheel
	} Mouse;

	void m_Update()
	{
		Keyboard.m_Update();
		Mouse.m_Update();
	}

	void m_EventUpdate(SDL_Event event)
	{
		Keyboard.m_EventUpdate(event);
		Mouse.m_EventUpdate(event);
	}
};

}
}

#endif /* RADENGINE_MODULE_INPUT_INPUT_HPP_ */
