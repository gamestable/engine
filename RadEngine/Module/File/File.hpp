/*
 * FileModule.hpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MODULE_FILE_FILE_HPP_
#define RADENGINE_MODULE_FILE_FILE_HPP_

#include <RadEngine/Module/Resource/Resource.hpp>

namespace rad
{
namespace engine
{

class CFile
{
public:
	CResourceLoader Resource;

	//Filename
	static std::string Basename( std::string const& pathname );
	static std::string RemoveExtension( std::string const& filename );
	static std::string GetExtension(std::string const& file);
	static std::string FileNameToName(std::string const& fileName);
};

class C
{

};

}
}

#endif /* RADENGINE_MODULE_FILE_FILE_HPP_ */
