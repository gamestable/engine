/*
 * File.cpp
 *
 *  Created on: Jan 27, 2017
 *      Author: Steffen Kremer
 */

#include "File.hpp"
#include <algorithm>
#include "Shlwapi.h"

namespace rad
{
namespace engine
{

struct MatchPathSeparator
{
    bool operator()( char ch ) const
    {
        return ch == '\\' || ch == '/';
    }
};

std::string CFile::Basename(std::string const& pathname)
{
	return std::string(std::find_if( pathname.rbegin(), pathname.rend(),MatchPathSeparator()).base(),pathname.end());
}

std::string CFile::RemoveExtension(std::string const& filename)
{
	std::string::const_reverse_iterator pivot = std::find(filename.rbegin(),filename.rend(),'.');
	return pivot == filename.rend()
			? filename
					: std::string( filename.begin(), pivot.base() - 1 );
}

std::string CFile::GetExtension(std::string const& file)
{
	return std::string(PathFindExtension(file.c_str()));
}

struct to_lower {
  int operator() ( int ch )
  {
    return std::tolower ( ch );
  }
};

std::string CFile::FileNameToName(std::string const& fileName)
{
	std::string tmp = RemoveExtension(Basename(fileName));
	std::transform(tmp.begin(), tmp.end(), tmp.begin(), to_lower());
	return tmp;
}

}
}
