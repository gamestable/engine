#include "Color.hpp"
#include <algorithm>

namespace rad
{
namespace engine
{

const ColorC ColorC::Black(0, 0, 0);
const ColorC ColorC::White(255, 255, 255);
const ColorC ColorC::Red(255, 0, 0);
const ColorC ColorC::Green(0, 255, 0);
const ColorC ColorC::Blue(0, 0, 255);
const ColorC ColorC::Yellow(255, 255, 0);
const ColorC ColorC::Magenta(255, 0, 255);
const ColorC ColorC::Cyan(0, 255, 255);
const ColorC ColorC::Transparent(0, 0, 0, 0);
const ColorC ColorC::Opaque(0, 0, 0, 255);
const ColorC ColorC::All(255, 255, 255, 255);

ColorC::ColorC() :
r(0),
g(0),
b(0),
a(255)
{

}

ColorC::ColorC(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) :
r(red),
g(green),
b(blue),
a(alpha)
{

}

ColorC::ColorC(unsigned int color) :
r((color & 0xff000000) >> 24),
g((color & 0x00ff0000) >> 16),
b((color & 0x0000ff00) >> 8 ),
a((color & 0x000000ff) >> 0 )
{

}

unsigned int ColorC::ToInteger() const
{
    return (r << 24) | (g << 16) | (b << 8) | a;
}

bool operator ==(const ColorC& left, const ColorC& right)
{
    return (left.r == right.r) &&
           (left.g == right.g) &&
           (left.b == right.b) &&
           (left.a == right.a);
}

bool operator !=(const ColorC& left, const ColorC& right)
{
    return !(left == right);
}

ColorC operator +(const ColorC& left, const ColorC& right)
{
    return ColorC((unsigned char)(std::min(int(left.r) + right.r, 255)),
                 (unsigned char)(std::min(int(left.g) + right.g, 255)),
                 (unsigned char)(std::min(int(left.b) + right.b, 255)),
                 (unsigned char)(std::min(int(left.a) + right.a, 255)));
}

ColorC operator -(const ColorC& left, const ColorC& right)
{
    return ColorC((unsigned char)(std::max(int(left.r) - right.r, 0)),
                 (unsigned char)(std::max(int(left.g) - right.g, 0)),
                 (unsigned char)(std::max(int(left.b) - right.b, 0)),
                 (unsigned char)(std::max(int(left.a) - right.a, 0)));
}

ColorC operator *(const ColorC& left, const ColorC& right)
{
    return ColorC((unsigned char)(int(left.r) * right.r / 255),
                 (unsigned char)(int(left.g) * right.g / 255),
                 (unsigned char)(int(left.b) * right.b / 255),
                 (unsigned char)(int(left.a) * right.a / 255));
}

ColorC& operator +=(ColorC& left, const ColorC& right)
{
    return left = left + right;
}

ColorC& operator -=(ColorC& left, const ColorC& right)
{
    return left = left - right;
}

ColorC& operator *=(ColorC& left, const ColorC& right)
{
    return left = left * right;
}

const ColorF ColorF::Black(0.0f, 0.0f, 0.0f);
const ColorF ColorF::White(1.0f, 1.0f, 1.0f);
const ColorF ColorF::Red(1.0f, 0.0f, 0.0f);
const ColorF ColorF::Green(0.0f, 1.0f, 0.0f);
const ColorF ColorF::Blue(0.0f, 0.0f, 1.0f);
const ColorF ColorF::Yellow(1.0f, 1.0f, 0.0f);
const ColorF ColorF::Magenta(1.0f, 0.0f, 1.0f);
const ColorF ColorF::Cyan(0.0f, 1.0f, 1.0f);
const ColorF ColorF::Transparent(0.0f, 0.0f, 0.0f, 0.0f);
const ColorF ColorF::Opaque(0.0f, 0.0f, 0.0f, 1.0f);
const ColorF ColorF::All(1.0f, 1.0f, 1.0f, 1.0f);

ColorF::ColorF() :
r(0),
g(0),
b(0),
a(1.0f)
{

}

ColorF::ColorF(float red, float green, float blue, float alpha) :
r(red),
g(green),
b(blue),
a(alpha)
{

}

/*ColorF::ColorF(unsigned int color) :
r((color & 0xff000000) >> 24),
g((color & 0x00ff0000) >> 16),
b((color & 0x0000ff00) >> 8 ),
a((color & 0x000000ff) >> 0 )
{

}*/

/*unsigned int ColorF::ToInteger() const
{
    return (r << 24) | (g << 16) | (b << 8) | a;
}*/

ColorC ColorF::ToColorChar()
{
	ColorC tmp;
	tmp.r = (unsigned char)(255 * r);
	tmp.g = (unsigned char)(255 * g);
	tmp.b = (unsigned char)(255 * b);
	tmp.a = (unsigned char)(255 * a);
	return tmp;
}

bool operator ==(const ColorF& left, const ColorF& right)
{
    return (left.r == right.r) &&
           (left.g == right.g) &&
           (left.b == right.b) &&
           (left.a == right.a);
}

bool operator !=(const ColorF& left, const ColorF& right)
{
    return !(left == right);
}

ColorF operator +(const ColorF& left, const ColorF& right)
{
	return ColorF(left.r + right.r,
	    		left.g + right.g,
				left.b + right.b,
				left.a + right.a);
}

ColorF operator -(const ColorF& left, const ColorF& right)
{
    return ColorF(left.r - right.r,
    		left.g - right.g,
			left.b - right.b,
			left.a - right.a);
}

ColorF operator *(const ColorF& left, const ColorF& right)
{
    return ColorF((float)(int(left.r) * right.r / 1.0f),
                 (float)(int(left.g) * right.g / 1.0f),
                 (float)(int(left.b) * right.b / 1.0f),
                 (float)(int(left.a) * right.a / 1.0f));
}

ColorF& operator +=(ColorF& left, const ColorF& right)
{
    return left = left + right;
}

ColorF& operator -=(ColorF& left, const ColorF& right)
{
    return left = left - right;
}

ColorF& operator *=(ColorF& left, const ColorF& right)
{
    return left = left * right;
}

}
}
