#include "Rect.hpp"

namespace rad
{
namespace engine
{

/*template <typename T>
inline Rect<T>::Rect() :
x(0),
y(0),
w(0),
h(0)
{

}

template <typename T>
inline Rect<T>::Rect(T X, T Y, T W, T H) :
x(X),
y(Y),
w(W),
h(H)
{

}*/

template <typename T>
template <typename U>
inline Rect<T>::Rect(const Rect<U>& rect) :
x(static_cast<T>(rect.x)),
y(static_cast<T>(rect.y)),
w(static_cast<T>(rect.w)),
h(static_cast<T>(rect.h))
{
}

template <typename T>
inline void Rect<T>::Set(T x, T y, T w, T h)
{
	this->x = static_cast<T>(x);
	this->y = static_cast<T>(y);
	this->w = static_cast<T>(w);
	this->h = static_cast<T>(h);
}

template <typename T>
inline std::string Rect<T>::ToString()
{
	std::string ret = std::to_string(this->x);
	ret += "/" + std::to_string(this->y);
	return ret;
}

/*template <typename T>
inline Rect<T> operator -(const Rect<T>& right)
{
    return Rect<T>(-right.x, -right.y);
}

template <typename T>
inline Rect<T>& operator +=(Rect<T>& left, const Rect<T>& right)
{
    left.x += right.x;
    left.y += right.y;

    return left;
}

template <typename T>
inline Rect<T>& operator -=(Rect<T>& left, const Rect<T>& right)
{
    left.x -= right.x;
    left.y -= right.y;

    return left;
}

template <typename T>
inline Rect<T> operator +(const Rect<T>& left, const Rect<T>& right)
{
    return Rect<T>(left.x + right.x, left.y + right.y);
}

template <typename T>
inline Rect<T> operator -(const Rect<T>& left, const Rect<T>& right)
{
    return Rect<T>(left.x - right.x, left.y - right.y);
}

template <typename T>
inline Rect<T> operator *(const Rect<T>& left, T right)
{
    return Rect<T>(left.x * right, left.y * right);
}

template <typename T>
inline Rect<T> operator *(T left, const Rect<T>& right)
{
    return Rect<T>(right.x * left, right.y * left);
}

template <typename T>
inline Rect<T>& operator *=(Rect<T>& left, T right)
{
    left.x *= right;
    left.y *= right;

    return left;
}

template <typename T>
inline Rect<T> operator /(const Rect<T>& left, T right)
{
    return Rect<T>(left.x / right, left.y / right);
}

template <typename T>
inline Rect<T>& operator /=(Rect<T>& left, T right)
{
    left.x /= right;
    left.y /= right;

    return left;
}

template <typename T>
inline bool operator ==(const Rect<T>& left, const Rect<T>& right)
{
    return (left.x == right.x) && (left.y == right.y);
}

template <typename T>
inline bool operator !=(const Rect<T>& left, const Rect<T>& right)
{
    return (left.x != right.x) || (left.y != right.y);
}*/

}
}
