#ifndef ENGINE_DATATYPES_INDEX_HPP_
#define ENGINE_DATATYPES_INDEX_HPP_

#include <string>

namespace rad
{
namespace engine
{

typedef unsigned int Index;
const unsigned int NOF_INDEX = 0;
const unsigned int MAX_INDEX = (-1);

class CIndexBase
{
public:
//protected:
	Index m_index = NOF_INDEX;
	std::string m_name = "";
	unsigned int m_nameHash = 0;
};

class CGameObject;

struct CGameObjectEntry
{
	unsigned int index = 0;
	CGameObject *object = nullptr;
};

}
}

#endif
