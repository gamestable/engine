#ifndef ENGINE_DATATYPES_COLOR_HPP_
#define ENGINE_DATATYPES_COLOR_HPP_

#include <iostream>

namespace rad
{
namespace engine
{

class ColorC
{
public:
    ColorC();
    ColorC(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = 255);
    explicit ColorC(unsigned int color);

    unsigned int ToInteger() const;
    std::string ToString();

    static const ColorC Black;       ///< Black predefined color
    static const ColorC White;       ///< White predefined color
    static const ColorC Red;         ///< Red predefined color
    static const ColorC Green;       ///< Green predefined color
    static const ColorC Blue;        ///< Blue predefined color
    static const ColorC Yellow;      ///< Yellow predefined color
    static const ColorC Magenta;     ///< Magenta predefined color
    static const ColorC Cyan;        ///< Cyan predefined color
    static const ColorC Transparent; ///< Transparent (black) predefined color
    static const ColorC Opaque;      ///< AntiTransparent predefined color
    static const ColorC All;         ///< For Texture Draw use predefined color

    unsigned char r; ///< Red component
    unsigned char g; ///< Green component
    unsigned char b; ///< Blue component
    unsigned char a; ///< Alpha (opacity) component
};

bool operator ==(const ColorC& left, const ColorC& right);

bool operator !=(const ColorC& left, const ColorC& right);

ColorC operator +(const ColorC& left, const ColorC& right);

ColorC operator -(const ColorC& left, const ColorC& right);

ColorC operator *(const ColorC& left, const ColorC& right);

ColorC& operator +=(ColorC& left, const ColorC& right);

ColorC& operator -=(ColorC& left, const ColorC& right);

ColorC& operator *=(ColorC& left, const ColorC& right);

class ColorF
{
public:
    ColorF();
    ColorF(float red, float green, float blue, float alpha = 1.0f);
    explicit ColorF(unsigned int color);

    unsigned int ToInteger() const;
    std::string ToString();
    ColorC ToColorChar();

    static const ColorF Black;       ///< Black predefined color
    static const ColorF White;       ///< White predefined color
    static const ColorF Red;         ///< Red predefined color
    static const ColorF Green;       ///< Green predefined color
    static const ColorF Blue;        ///< Blue predefined color
    static const ColorF Yellow;      ///< Yellow predefined color
    static const ColorF Magenta;     ///< Magenta predefined color
    static const ColorF Cyan;        ///< Cyan predefined color
    static const ColorF Transparent; ///< Transparent (black) predefined color
    static const ColorF Opaque;      ///< AntiTransparent predefined color
    static const ColorF All;         ///< For Texture Draw use predefined color

    float r; ///< Red component
    float g; ///< Green component
    float b; ///< Blue component
    float a; ///< Alpha (opacity) component
};

bool operator ==(const ColorF& left, const ColorF& right);

bool operator !=(const ColorF& left, const ColorF& right);

ColorF operator +(const ColorF& left, const ColorF& right);

ColorF operator -(const ColorF& left, const ColorF& right);

ColorF operator *(const ColorF& left, const ColorF& right);

ColorF& operator +=(ColorF& left, const ColorF& right);

ColorF& operator -=(ColorF& left, const ColorF& right);

ColorF& operator *=(ColorF& left, const ColorF& right);

}
}

#endif
