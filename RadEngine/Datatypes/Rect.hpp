#ifndef ENGINE_DATATYPES_RECT_HPP_
#define ENGINE_DATATYPES_RECT_HPP_

#include <string>

namespace rad
{
namespace engine
{

template <typename T>
class Rect
{
public:
    Rect<T>():x(T()),y(T()),w(T()),h(T()) {};
    Rect<T>(T X, T Y, T W, T H):x(T(X)),y(T(Y)),w(T(W)),h(T(H)) {};
    template <typename U>
    explicit Rect<T>(const Rect<U>& vector);
	
	void Set(T x, T y, T w, T h);
	std::string ToString();

    T x;
    T y;
	T w;
	T h;
};

/*template <typename T>
Rect<T> operator -(const Rect<T>& right);

template <typename T>
Rect<T>& operator +=(Rect<T>& left, const Rect<T>& right);

template <typename T>
Rect<T>& operator -=(Rect<T>& left, const Rect<T>& right);

template <typename T>
Rect<T> operator +(const Rect<T>& left, const Rect<T>& right);

template <typename T>
Rect<T> operator -(const Rect<T>& left, const Rect<T>& right);

template <typename T>
Rect<T> operator *(const Rect<T>& left, T right);

template <typename T>
Rect<T> operator *(T left, const Rect<T>& right);

template <typename T>
Rect<T>& operator *=(Rect<T>& left, T right);

template <typename T>
Rect<T> operator /(const Rect<T>& left, T right);

template <typename T>
Rect<T>& operator /=(Rect<T>& left, T right);

template <typename T>
bool operator ==(const Rect<T>& left, const Rect<T>& right);

template <typename T>
bool operator !=(const Rect<T>& left, const Rect<T>& right);*/

typedef Rect<int> Recti;
typedef Rect<unsigned int> Rectu;
typedef Rect<float> Rectf;
typedef Rect<double> Rectd;

}
}

#endif
