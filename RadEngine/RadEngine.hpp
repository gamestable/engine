/*
 * RadEngine.hpp
 *
 *  Created on: 31.12.2016
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_RADENGINE_HPP_
#define RADENGINE_RADENGINE_HPP_

#include <iostream>
#include <vector>

//Base
#include "Debug/Debug.hpp"
//Container
#include "Container/GameState.hpp"
//Manager
#include "Manager/Display/Display.hpp"
#include "Manager/Resource/Resource.hpp"
//Modules
#include "Module/File/File.hpp"
#include "Module/Input/Input.hpp"
#include "Module/Math/Math.hpp"
#include "Module/OS/OS.hpp"
#include "Module/Time/Time.hpp"
//Runtime Managers

//Game
#include "Game/Game.hpp"

namespace rad
{
namespace engine
{

class CEngine
{
public:
	bool Init(int argc, char *argv[]);
	void Destroy();

	void ChangeState(CGameState *state);
	void PushState(CGameState *state);
	void PopState();
	CGameState* GetTopState();
	std::string GetStates();

	void HandleEvents();
	void Update();
	void Draw();

	std::string GetVersion() {return std::to_string(m_version[0])+"."+std::to_string(m_version[1])+"."+std::to_string(m_version[2]);}

	bool IsRunning() {return m_running;}
	void Quit() {m_running = false;}

	//THIS ORDER IDIOT!
	//Debug Quitter Libraries etc
	//Manager
	//Modules
	//Runtime Objects -> Containers
	//For safe engine destruction!

	CDebug Debug;
	class Quitter
	{
	public:
		~Quitter() {}
	} m_quitter;

	CDisplay Display;
	CInput Input;
	CResource Resource;
	CFile File;
	CMath Math;
	COS OS;
	CTime Time;
	CResourceLoader Loader;

	/*DisplayManager display;
	InputModule input;
	ResourceManager resource;
	ShaderManager shader;
	ScriptManager script;
	GUIManager gui;

	FileModule file;
	TimeModule time;

	ObjectManager object;*/

private:
	bool m_LoadStandardData();

	std::vector<CGameState *> m_states;
	bool m_running = true;
	char m_version[3] {0,0,4};

	SDL_Event m_sdlEvent;
};

extern CEngine RadEngine;

extern CEngine *Engine;
extern CDebug *Debug;
extern CDisplay *Display;
extern CInput *Input;
extern CResource *Resource;
extern CFile *File;
extern CMath *Math;
extern COS *OS;
extern CTime *Time;
extern CResourceLoader *Loader;

}
}

#endif /* RADENGINE_RADENGINE_HPP_ */
