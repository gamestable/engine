/*
 * RadEngine.cpp
 *
 *  Created on: 31.12.2016
 *      Author: Steffen Kremer
 */

#include "RadEngine.hpp"
#include "RadEngine/Extern/IncludeFT.hpp"

//Standard Data
#include "Data/DataFontStandard.hpp"
#include "Data/DataTextureLoadingScreen.hpp"

namespace rad
{
namespace engine
{

bool CEngine::Init(int argc, char *argv[])
{
	if (argc>1)
	{
		Debug.Log("Engine started with following parameters:");
		for (int i=1;i<argc;i++)
			Debug.Log(std::string(argv[i]));
	}
	else
	{
		Debug.Log("Engine started...");
	}
	if (SDL_Init(SDL_INIT_TIMER|SDL_INIT_JOYSTICK|SDL_INIT_HAPTIC|SDL_INIT_GAMECONTROLLER|SDL_INIT_EVENTS)!=0)
	{
		Debug.Error("Can't init basic SDL!");
		return false;
	}
	SDL_DisableScreenSaver();
	Debug.Log("SDL "+std::to_string(SDL_COMPILEDVERSION)+" inited...");
	if (!Display.Init(DisplayResolution()))
	{
		Debug.Error("Can't open Display!");
		return false;
	}
	if (!Resource.Sound.Init())
	{
		Debug.Error("Can't init Sound!");
		return false;
	}
	Debug.Log("OpenAL inited...");
	/*if (!Script.Init())
	{
		Debug.Error("Cant init script support!");
		return false;
	}*/
	Debug.Log("Scripting inited...");
	/*if (m_LoadData())
	{
		Debug.Error("LoadData error!");
		return false;
	}*/
	Debug.Log("Engine inited...");
	if (!m_LoadStandardData())
		return false;

	Debug.Log("Creating states...");

	Debug.Log("Entering main loop...");

	Display.Fps.Timer.Start();
	return true;
}

void CEngine::Destroy()
{
	Debug.Log("Destroying Engine...");
	Debug.Log("Freeing States:");
	Debug.Log(GetStates());
	while(!m_states.empty())
	{
		m_states.back()->DestroyObjects();
		m_states.back()->Destroy();
		m_states.pop_back();
	}
	Debug.Log("Destroying Resources...");
	Resource.Texture.Destroy();
	Resource.Font.Destroy();
	Resource.Sound.Destroy();
	Debug.Log("Freeing FreeType...");
	Debug.Log("Destroying Display...");
	Display.Destroy();
	Debug.Log("Quitting SDL...");
	SDL_EnableScreenSaver();
	SDL_Quit();
	Debug.Log("Finished!");
}

void CEngine::ChangeState(CGameState *state)
{
	if (!m_states.empty())
	{
		m_states.front()->DestroyObjects();
		m_states.front()->Destroy();
		m_states.erase(m_states.begin());
	}
	m_states.insert(m_states.begin(), state);
	m_states.front()->InitObjects();
	m_states.front()->Init();
}

void CEngine::PushState(CGameState *state)
{
	if (!m_states.empty())
	{
		m_states.front()->Pause();
	}
	m_states.insert(m_states.begin(), state);
	m_states.front()->InitObjects();
	m_states.front()->Init();
}

void CEngine::PopState()
{
	if (!m_states.empty())
	{
		m_states.front()->DestroyObjects();
		m_states.front()->Destroy();
		m_states.erase(m_states.begin());
	}
	if (!m_states.empty())
	{
		m_states.front()->Resume();
	}
}

CGameState* CEngine::GetTopState()
{
	if (!m_states.empty())
	{
		return m_states.back();
	}
	Debug.Warning("GetTopState empty!");
	return nullptr;
}

std::string CEngine::GetStates()
{
	if (!m_states.empty())
	{
		std::string tmp;
		for (unsigned int i=0;i<m_states.size();++i)
		{
			tmp += "* " + m_states.at(i)->GetName();
		}
		return tmp;
	}
	return "Empty";
}

void CEngine::HandleEvents()
{
	while (SDL_PollEvent(&m_sdlEvent))
	{
		switch(m_sdlEvent.type)
		{
		case SDL_KEYDOWN:
		case SDL_KEYUP:
		case SDL_MOUSEMOTION:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEWHEEL:
			Input.m_EventUpdate(m_sdlEvent);
			break;
		case SDL_QUIT:
			m_running = false;
			break;
		}
	}
}

void CEngine::Update()
{
	Time.Update();
	Display.Fps.CapTimer.Start();
	if (Time.SinceStart()>=(int)(4294967291/2)-10000)
	{
		Debug.Crash();
	}
	HandleEvents();
	if (!m_running)
	{
		return;
	}
	//Resource.Sound.Update();
	Display.Update();
	if (m_states.front()->UpdateUnderlying())
	{
		if (m_states.size()>1)
		{
			m_states.at(m_states.size()-2)->Update();
			m_states.at(m_states.size()-2)->UpdateObjects();
		}
	}
	m_states.front()->UpdateObjects();
	m_states.front()->Update();
	Display.Clear();
	Input.m_Update();
}

void CEngine::Draw()
{
	if (m_states.front()->DrawUnderlying())
	{
		if (m_states.size()>1)
		{
			m_states.at(m_states.size()-2)->Draw();
			m_states.at(m_states.size()-2)->DrawObjects();
		}
	}
	m_states.front()->DrawObjects();
	m_states.front()->Draw();
	Display.Swap();
}

bool CEngine::m_LoadStandardData()
{
	Debug.Log("Loading Standard Resources...");
	CResourceTexture * tmp = new CResourceTexture;
	bool success = tmp->LoadFromImageData(std::string(DataTextureLoadingScreen_Name), (void *)(DataTextureLoadingScreen), DataTextureLoadingScreen_size);
	if (success==false)
	{
		Debug.Warning("Cant load LoadingScreen Texture!");
	}
	CResourceFont * tmp2 = new CResourceFont;
	//for (unsigned int o=10; o<=45&&success; o+=1)
	//{
		success = success&&tmp2->LoadFromMemory(std::string(DataFontStandard_Name), 25, (void *)DataFontStandard,DataFontStandard_size);
	//}
	if (success==false)
	{
		Debug.Warning("Cant load Standard font!");
	}
	success = Loader.OpenContainer("./data/base.bin");
	if (success==false)
	{
		Debug.Warning("Cant open base.bin!");
	}
	Loader.LoadResources(1);
	return true;
}

CEngine RadEngine;

CEngine *Engine = &RadEngine;
CDebug *Debug = &RadEngine.Debug;
CDisplay *Display = &RadEngine.Display;
CInput *Input = &RadEngine.Input;
CResource *Resource = &RadEngine.Resource;
CFile *File = &RadEngine.File;
CMath *Math = &RadEngine.Math;
COS *OS = &RadEngine.OS;
CTime *Time = &RadEngine.Time;
CResourceLoader *Loader = &RadEngine.Loader;

}
}
