/*
 * IncludeLUA5.hpp
 *
 *  Created on: Jan 31, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_EXTERN_INCLUDELUA5_HPP_
#define RADENGINE_EXTERN_INCLUDELUA5_HPP_


namespace rad
{
namespace engine
{

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

}
}

#endif /* RADENGINE_EXTERN_INCLUDELUA5_HPP_ */
