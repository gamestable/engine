/*
 * IncludeFT.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#include <ft2build.h>
#include FT_FREETYPE_H
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>

#ifndef FT_EXTRA_GETERROR_
#define FT_EXTRA_GETERROR_

const char* FT_Extra_GetErrorMessage(FT_Error err);

#endif
