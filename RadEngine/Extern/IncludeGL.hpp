/*
 * IncludeGL.hpp
 *
 *  Created on: Jan 20, 2017
 *      Author: Steffen Kremer
 */

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glut.h>
#include "SDL2/SDL_opengl.h"
#undef main
#include <GL/GLU.h>
#include <IL/IL.h>
#include <IL/ilu.h>
