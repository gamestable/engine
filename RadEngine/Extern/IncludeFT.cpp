/*
 * IncludeFT.cpp
 *
 *  Created on: Feb 8, 2017
 *      Author: Steffen Kremer
 */

#include "IncludeFT.hpp"

const char* FT_Extra_GetErrorMessage(FT_Error err)
{
    #undef __FTERRORS_H__
    #define FT_ERRORDEF( e, v, s )  case e: return s;
    #define FT_ERROR_START_LIST     switch (err) {
    #define FT_ERROR_END_LIST       }
    #include FT_ERRORS_H
    return "Unknown error";
}
