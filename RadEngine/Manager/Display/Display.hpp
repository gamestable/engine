/*
 * Display.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MANAGER_DISPLAY_DISPLAY_HPP_
#define RADENGINE_MANAGER_DISPLAY_DISPLAY_HPP_

#include "../../Extern/IncludeSDL.hpp"
#include "../../Module/Time/Timer.hpp"
#include "DisplayData.hpp"

namespace rad
{
namespace engine
{

class CDisplay
{
public:
	class CCamera
	{
	public:
		double GetX() {return m_cameraX;}
		double GetY() {return m_cameraY;}
		void Set(double x, double y);
		void SetNoChange(double x, double y);
		void SetX(double x);
		void SetY(double y);
		void SwitchCameraMode();
	private:
		double m_cameraX = 0;
		double m_cameraY = 0;
		double m_cameraXScreenDraw = 0;
		double m_cameraYScreenDraw = 0;
		void m_Changed();
	} Camera;

	bool Init(DisplayResolution setting);
	void Destroy();

	void Update();
	void Clear();
	void Swap();

	DisplayResolution screen;
	DisplayResolution viewport;
	DisplayResolution native;

	DisplayFPSCalculation Fps;

private:
	bool m_InitGL();
	DisplayAspectRatio m_CalculateAspectRatio(unsigned int x, unsigned int y);

	SDL_Window *m_sdlWindow = nullptr;
	SDL_GLContext m_GlContext = nullptr;

	bool m_isDestroyed = false;
};

}
}

#endif /* RADENGINE_MANAGER_DISPLAY_DISPLAY_HPP_ */
