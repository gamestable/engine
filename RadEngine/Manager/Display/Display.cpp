/*
 * Display.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#include "Display.hpp"
#include "../../Extern/IncludeGL.hpp"
#include "../../RadEngine.hpp"
#include "../../Game/Game.hpp"
#include "Data/DataWindowIcon.hpp"

namespace rad
{
namespace engine
{

bool CDisplay::Init(DisplayResolution setting)
{
	native.sizeX = RadEngine.OS.Screen.GetSizeX();
	native.sizeY = RadEngine.OS.Screen.GetSizeY();
	//native.sizeX = 800;
	//native.sizeY = 600;
	//native.sizeX = native.sizeX / 2;
	//native.sizeY = native.sizeY / 2;
	//screen.fullscreen = false;
	native.aspectRatio = m_CalculateAspectRatio(native.sizeX, native.sizeY);
	if (native.aspectRatio==DisplayAspectRatio::UNKNOWN)
	{
		RadEngine.Debug.Warning("Unknown aspect ratio!");
	}
	if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
	{
		RadEngine.Debug.Error("Can't init SDL!");
		return false;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	{
		unsigned int flags = 0;
		if (screen.fullscreen) {
			flags = SDL_WINDOW_FULLSCREEN | SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
		} else {
			flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
		}
		m_sdlWindow = SDL_CreateWindow(Game.GetTitle().c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, native.sizeX,native.sizeY, flags);
	}
	if (m_sdlWindow==nullptr)
	{
		RadEngine.Debug.Error("Can't create window!");
		return false;
	}
	m_GlContext = SDL_GL_CreateContext(m_sdlWindow);
	if (m_GlContext==nullptr)
	{
		RadEngine.Debug.Error("Can't create GL context!");
		return false;
	}
	{
		GLenum status = glewInit();
		if (status!=GLEW_OK)
		{
			RadEngine.Debug.Error("Can't init GLEW! Error: "+std::string((char*)glewGetErrorString(status)));
			return false;
		}
	}
	if (!GLEW_VERSION_2_1)
	{
		RadEngine.Debug.Error("GLEW OpenGL 2.1 is not supported!");
		return false;
	}
	if(SDL_GL_SetSwapInterval(1)<0)
	{
		RadEngine.Debug.Warning("Unable to use VSYNC!");
	}
	RadEngine.Debug.Log("Display inited...");
	{
		SDL_Surface *surface;
		surface = SDL_CreateRGBSurfaceFrom(DataWindowIcon,16,16,16,16*2,0x0f00,0x00f0,0x000f,0xf000);
		SDL_SetWindowIcon(m_sdlWindow, surface);
		SDL_FreeSurface(surface);
	}
	RadEngine.Debug.Log("Window icon loaded...");
	return m_InitGL();
}

void CDisplay::Destroy()
{
	SDL_GL_DeleteContext(m_GlContext);
	SDL_DestroyWindow(m_sdlWindow);
}

void CDisplay::Update()
{
	unsigned int tStamp = SDL_GetTicks();
	if (tStamp-Fps.lastTimeStamp>=1000||Fps.lastTimeStamp==0)
	{
		Fps.lastTimeStamp = tStamp;
		Fps.current = Fps.countedFrames;
		Fps.average = (Fps.average + Fps.current)/2;
		Fps.countedFrames = 0;
		//std::cout << Fps.current << std::endl;
	}
}

void CDisplay::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT);
	//Pop default matrix onto current matrix
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	//Save default matrix again
	glPushMatrix();
}

void CDisplay::Swap()
{
	SDL_GL_SwapWindow(m_sdlWindow);
	Fps.countedFrames++;

	/*int frameTicks = Fps.CapTimer.GetTicks();
	if ((unsigned int)frameTicks < Fps.screenTicks)
	{
		if ((1000/Fps.target)>(Fps.Timer.GetTicks()-Fps.CapTimer.GetTicks()))
		{
			//Only with no vsync
			//Both not working
			//SDL_Delay(this->display.fps.screenTicks - frameTicks);
			//SDL_Delay((1000/this->display.fps.target)-(this->display.fps.timer.getTicks()-this->display.fps.capTimer.getTicks()));
		}
	}*/ //Useless with Time.delta?
}

bool CDisplay::m_InitGL()
{
	float vrx, vry, rx, ry, vpsx = 0, vpsy = 0, vppx = 0, vppy = 0;
	vrx = screen.sizeX;
	vry = screen.sizeY;
	rx = native.sizeX;
	ry = native.sizeY;
	if ((unsigned int)((vrx/vry)*100)!=(unsigned int)((rx/ry)*100))
	{
		float targetAspectRatio = vrx/vry;
		vpsx = native.sizeX;
		vpsy = (int)(vpsx / targetAspectRatio + 0.5f);
		vppx = (rx - vpsx)/2;
		vppy = (ry - vpsy)/2;
		native.offsetX = vppx;
		native.offsetY = vppy;
	}
	else //Same aspect ratio
	{
		vpsx = rx;
		vpsy = ry;
		vppx = 0;
		vppy = 0;
		native.offsetX = vppx;
		native.offsetY = vppy;
	}
	viewport.sizeX = vpsx;
	viewport.sizeY = vpsy;
	viewport.aspectRatio = m_CalculateAspectRatio(viewport.sizeX,viewport.sizeY);
	glViewport(vppx,vppy,vpsx,vpsy);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	GLenum error = glGetError();
	if (error!=GL_NO_ERROR)
	{
		RadEngine.Debug.Error("glMatrix/LoadIdentity(PROJ) - "+std::string((char*)gluErrorString(error)));
		return false;
	}
	glOrtho(0.0,screen.sizeX,screen.sizeY,0.0,-1.0,1.0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	error = glGetError();
	if (error!=GL_NO_ERROR)
	{
		RadEngine.Debug.Error("glMatrix/LoadIdentity(MODEL) - "+std::string((char*)gluErrorString(error)));
		return false;
	}
	glClearColor(0.f,0.f,0.f,1.f);
	error = glGetError();
	if (error!=GL_NO_ERROR)
	{
		RadEngine.Debug.Error("glClearColor - "+std::string((char*)gluErrorString(error)));
		return false;
	}
	glEnable( GL_TEXTURE_2D );
	//Initialize DevIL
	ilInit();
	ilClearColour(0,0,0,0);
	{
		ILenum ilError = ilGetError();
		if(ilError!=IL_NO_ERROR)
		{
			RadEngine.Debug.Error("DevIL Init Fail! Code: "+std::to_string(ilError));
			return false;
		}
	}
	RadEngine.Debug.Log("OpenGL inited...");
	return true;
}

DisplayAspectRatio CDisplay::m_CalculateAspectRatio(unsigned int x, unsigned int y)
{
	if ((3 * y)/100 == (2 * x)/100) //3:2
	{
		return DisplayAspectRatio::THREETWO;
	}
	else if ((4 * y)/100 == (3 * x)/100) //4:3
	{
		return DisplayAspectRatio::FOURTHREE;
	}
	else if ((16 * y)/100 == (9 * x)/100) //16:9
	{
		return DisplayAspectRatio::SIXTEENNINE;
	}
	else if ((16 * y)/100 == (10 * x)/100) //16:10
	{
		return DisplayAspectRatio::SIXTEENTEN;
	}
	else if ((5 * y)/100 == (4 * x)/100) //5:4
	{
		return DisplayAspectRatio::FIVEFOUR;
	}
	return DisplayAspectRatio::UNKNOWN;
}

//######################################

void CDisplay::CCamera::Set(double x, double y)
{
	m_cameraX = x;
	m_cameraY = y;
	m_Changed();
}

void CDisplay::CCamera::SetNoChange(double x, double y)
{
	m_cameraX = x;
	m_cameraY = y;
}

void CDisplay::CCamera::SwitchCameraMode()
{
	float cx, cy;
	cx = m_cameraX;
	cy = m_cameraY;
	m_cameraX = m_cameraXScreenDraw;
	m_cameraY = m_cameraYScreenDraw;
	m_cameraXScreenDraw = cx;
	m_cameraYScreenDraw = cy;
	m_Changed();
}

void CDisplay::CCamera::m_Changed()
{
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glLoadIdentity();
	//Move camera to position
	glTranslatef(m_cameraX, m_cameraY, 0.f);
	//Save default matrix again with camera translation
	glPushMatrix();
}

}
}
