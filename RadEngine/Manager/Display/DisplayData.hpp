/*
 * DisplayData.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MANAGER_DISPLAY_DISPLAYDATA_HPP_
#define RADENGINE_MANAGER_DISPLAY_DISPLAYDATA_HPP_


namespace rad
{
namespace engine
{

typedef enum
{
	UNKNOWN = 0,
	THREETWO,
	FOURTHREE,
	FIVEFOUR,
	SIXTEENNINE,
	SIXTEENTEN
} DisplayAspectRatio;

struct DisplayResolution
{
	bool fullscreen = false;
	unsigned int sizeX = 1920;
	unsigned int sizeY = 1080;
	unsigned int offsetX = 0;
	unsigned int offsetY = 0;
	DisplayAspectRatio aspectRatio = DisplayAspectRatio::FOURTHREE;
};

struct DisplayFPSCalculation
{
	double target = 60.f;
	double average = 60.f;
	double current = 60.f;
	unsigned int lastTimeStamp = 0;
	unsigned int currentFrame = 0;
	unsigned int countedFrames = 0;
	unsigned int screenTicks = 1000 / target;
	CTimer Timer;
	CTimer CapTimer;
};

}
}

#endif /* RADENGINE_MANAGER_DISPLAY_DISPLAYDATA_HPP_ */
