/*
 * Resource.hpp
 *
 *  Created on: Jan 31, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MANAGER_RESOURCE_RESOURCE_HPP_
#define RADENGINE_MANAGER_RESOURCE_RESOURCE_HPP_

#include "Texture.hpp"
#include "Font.hpp"
#include "Sound.hpp"
#include "../../Extern/IncludeAL.hpp"
#include <vector>

namespace rad
{
namespace engine
{

class CResource
{
public:
	class CTextureManager
	{
	public:
		void Destroy()
		{
			m_isDestructing = true;
			for (unsigned int i=0;i<m_texture.size();i++)
				if (m_texture.at(i)!=nullptr)
					delete m_texture.at(i);
		}
		unsigned int Add(CResourceTexture* obj)
		{
			m_texture.push_back(obj);
			return m_texture.size();
		}
		CResourceTexture* Get(unsigned int index)
		{
			return m_texture.at(index-1);
		}
		void Remove(CResourceTexture* obj) //Better use SetNull
		{
			if (!m_isDestructing)
				m_texture.erase(m_texture.begin() + Find(obj) - 1);
		}
		void Remove(unsigned int objIndex)
		{
			if (!m_isDestructing)
				m_texture.erase(m_texture.begin() + objIndex - 1);
		}
		unsigned int Find(CResourceTexture* obj)
		{
			for (unsigned int i=0;i<m_texture.size();i++)
				if (m_texture.at(i)==obj)
					return i+1;
			return 0;
		}
		unsigned int Find(std::string name)
		{
			for (unsigned int i=0;i<m_texture.size();i++)
				if (m_texture.at(i)->GetName()==name)
					return i+1;
			return 0;
		}
		unsigned int Find(unsigned int hashId)
		{
			for (unsigned int i=0;i<m_texture.size();i++)
				if (m_texture.at(i)->GetHash()==hashId)
					return i+1;
			return 0;
		}
		bool IsDestructing() {return m_isDestructing;}
	private:
		std::vector<CResourceTexture*> m_texture;
		bool m_isDestructing = false;
	} Texture;

	class CResourceFontManager
	{
	public:
		void Destroy()
		{
			m_isDestructing = true;
			for (unsigned int i=0;i<m_font.size();i++)
				if (m_font.at(i)!=nullptr)
					delete m_font.at(i);
		}
		unsigned int Add(CResourceFont* obj)
		{
			m_font.push_back(obj);
			return m_font.size();
		}
		CResourceFont* Get(unsigned int index)
		{
			return m_font.at(index-1);
		}
		void Remove(CResourceFont* obj)
		{
			if (!m_isDestructing)
				m_font.erase(m_font.begin() + Find(obj) - 1);
		}
		void Remove(unsigned int objIndex)
		{
			if (!m_isDestructing)
				m_font.erase(m_font.begin() + objIndex - 1);
		}
		unsigned int Find(CResourceFont* obj)
		{
			for (unsigned int i=0;i<m_font.size();i++)
				if (m_font.at(i)==obj)
					return i+1;
			return 0;
		}
		bool IsDestructing() {return m_isDestructing;}
	private:
		std::vector<CResourceFont*> m_font;
		bool m_isDestructing = false;
	} Font;

	class CSoundManager
	{
	public:
		bool Init();
		void Destroy();

		unsigned int Add(CResourceSound* obj)
		{
			m_sound.push_back(obj);
			return m_sound.size();
		}
		CResourceSound* Get(unsigned int index)
		{
			return m_sound.at(index-1);
		}
		void Remove(CResourceSound* obj)
		{
			if (!m_isDestructing)
				m_sound.erase(m_sound.begin() + Find(obj) - 1);
		}
		void Remove(unsigned int objIndex)
		{
			if (!m_isDestructing)
				m_sound.erase(m_sound.begin() + objIndex - 1);
		}
		unsigned int Find(CResourceSound* obj)
		{
			for (unsigned int i=0;i<m_sound.size();i++)
				if (m_sound.at(i)==obj)
					return i+1;
			return 0;
		}
		bool IsDestructing() {return m_isDestructing;}
	private:
		std::vector<CResourceSound*> m_sound;
		ALCdevice *m_alDevice = nullptr;
		ALCcontext *m_alContextGUI = nullptr;
		ALCcontext *m_alContext = nullptr;
		bool m_isDestructing = false;
	} Sound;

	class CAnimationManager
	{
	public:

	private:
		//std::vector<CResourceAnimation*> m_animation;
	};

private:
};

}
}

#endif /* RADENGINE_MANAGER_RESOURCE_RESOURCE_HPP_ */
