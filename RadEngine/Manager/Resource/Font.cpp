/*
 * Font.cpp
 *
 *  Created on: Feb 7, 2017
 *      Author: Steffen Kremer
 */

#include "Font.hpp"
#include "RadEngine/Extern/IncludeGL.hpp"
#include "RadEngine/RadEngine.hpp"

namespace rad
{
namespace engine
{

CResourceFont::CResourceFont()
{
	m_index = RadEngine.Resource.Font.Add(this);
}

CResourceFont::~CResourceFont()
{
	Destroy();
	RadEngine.Resource.Font.Remove(m_index);
}

bool CResourceFont::LoadFromFace(std::string file, FT_Library lib, FT_Face face, unsigned int fontSize)
{
	if (fontSize<5)
	{
		Debug->Warning("Font size for "+file+" too small!");
		return false;
	}
	int error = 0;
	for (unsigned char c=0;c<255;c++)
	{
		if(FT_Load_Glyph( face, FT_Get_Char_Index( face, c ), FT_LOAD_DEFAULT ))
		{
			RadEngine.Debug.Warning("Cant load FreeType Glyph! FT_Error: "+std::string(FT_Extra_GetErrorMessage(error)));
			break;
		}
		else
		{
			// Move The Face's Glyph Into A Glyph Object.
			FT_Glyph glyph;
			if(FT_Get_Glyph( face->glyph, &glyph ))
			{
				Debug->Error("Cant get glyph "+std::to_string(c)+" from "+file);
				continue;
			}

			// Convert The Glyph To A Bitmap.
			if (FT_Glyph_To_Bitmap( &glyph, ft_render_mode_normal, 0, 1 ))
			{
				Debug->Error("Cant convert glyph "+std::to_string(c)+" to bitmap from "+file);
				continue;
			}
			FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;
			FT_Bitmap& bitmap=bitmap_glyph->bitmap;

			int width = RadEngine.Math.PowerOfTwo( bitmap.width );
			int height = RadEngine.Math.PowerOfTwo( bitmap.rows );

			// Allocate Memory For The Texture Data.
			GLubyte* expanded_data = new GLubyte[ 2 * width * height];

			// Here We Fill In The Data For The Expanded Bitmap.
			// Notice That We Are Using A Two Channel Bitmap (One For
			// Channel Luminosity And One For Alpha), But We Assign
			// Both Luminosity And Alpha To The Value That We
			// Find In The FreeType Bitmap.
			// We Use The ?: Operator To Say That Value Which We Use
			// Will Be 0 If We Are In The Padding Zone, And Whatever
			// Is The FreeType Bitmap Otherwise.
			for(unsigned int j = 0; j <(unsigned int)height ; j++) {
			  for(unsigned int i = 0; i < (unsigned int)width; i++) {
			    expanded_data[2 * (i + j * (unsigned int)width)] = 255;
			    expanded_data[2 * (i + j * (unsigned int)width) + 1] =
			        (i >= bitmap.width || j >= bitmap.rows) ? 0 : bitmap.buffer[i + bitmap.width * j];
			  }
			}

			//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glGenTextures(1, &m_character[c].textureID);

			glBindTexture( GL_TEXTURE_2D, m_character[c].textureID);
			//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			//glTexImage2D(GL_TEXTURE_2D,0, GL_RGBA,face->glyph->bitmap.width,face->glyph->bitmap.rows,0, GL_ALPHA,GL_UNSIGNED_BYTE,face->glyph->bitmap.buffer);
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data );
			delete [] expanded_data;

			m_character[c].advance = Vector2<int> (face->glyph->advance.x,face->glyph->advance.y);
			m_character[c].size = Vector2<unsigned int> (width,height);
			m_character[c].bearing = Vector2<int> (face->glyph->bitmap_left,face->glyph->bitmap_top);

			glBindTexture(GL_TEXTURE_2D, this->m_character[c].textureID);

			glPushMatrix();

			// First We Need To Move Over A Little So That
			// The Character Has The Right Amount Of Space
			// Between It And The One Before It.
			glTranslatef(bitmap_glyph->left,0,0);

			// Now We Move Down A Little In The Case That The
			// Bitmap Extends Past The Bottom Of The Line
			// This Is Only True For Characters Like 'g' Or 'y'.
			glTranslatef(0,bitmap_glyph->top-bitmap.rows,0);

			// Now We Need To Account For The Fact That Many Of
			// Our Textures Are Filled With Empty Padding Space.
			// We Figure What Portion Of The Texture Is Used By
			// The Actual Character And Store That Information In
			// The x And y Variables, Then When We Draw The
			// Quad, We Will Only Reference The Parts Of The Texture
			// That Contains The Character Itself.
			float   x=(float)bitmap.width / (float)width,
			y=(float)bitmap.rows / (float)height;

			// Here We Draw The Texturemapped Quads.
			// The Bitmap That We Got From FreeType Was Not
			// Oriented Quite Like We Would Like It To Be,
			// But We Link The Texture To The Quad
			// In Such A Way That The Result Will Be Properly Aligned.
			glBegin(GL_QUADS);
			glTexCoord2d(0,0); glVertex2f(0,bitmap.rows);
			glTexCoord2d(0,y); glVertex2f(0,0);
			glTexCoord2d(x,y); glVertex2f(bitmap.width,0);
			glTexCoord2d(x,0); glVertex2f(bitmap.width,bitmap.rows);
			glEnd();
			glPopMatrix();
			glTranslatef(face->glyph->advance.x >> 6 ,0,0);

		}
	}
	m_name = RadEngine.File.FileNameToName(file);
	m_file = file;
	m_height = fontSize;
	return true;
}

bool CResourceFont::LoadFromFile(std::string file, unsigned int fontSize)
{
	if (fontSize<5)
	{
		Debug->Warning("Font size for "+file+" too small!");
		return false;
	}
	Renew();
	FT_Library lib;
	int error = FT_Init_FreeType(&lib);
	if (error)
	{
		RadEngine.Debug.Error("Cant int FreeType Lib! FT_Error: "+std::string(FT_Extra_GetErrorMessage(error)));
		return false;
	}
	FT_Face face;
	error = FT_New_Face(lib,file.c_str(),0,&face);
	if (error)
	{
		RadEngine.Debug.Warning("Cant create FreeType Face! FT_Error: "+std::string(FT_Extra_GetErrorMessage(error)));
		return false;
	}
	//FT_Set_Pixel_Sizes(face, 0, fontSize);
	FT_Set_Char_Size( face, fontSize << 6, fontSize << 6, 96, 96);
	bool ret = LoadFromFace(file, lib, face, fontSize);
	FT_Done_Face(face);
	FT_Done_FreeType(lib);
	return ret;
}

bool CResourceFont::LoadFromMemory(std::string file, unsigned int fontSize, void *data, unsigned int dataSize)
{
	if (fontSize<5)
	{
		return false;
	}
	Renew();
	FT_Library lib;
	int error = FT_Init_FreeType(&lib);
	if (error)
	{
		RadEngine.Debug.Error("Cant int FreeType Lib! FT_Error: "+std::string(FT_Extra_GetErrorMessage(error)));
		return false;
	}
	FT_Face face;
	error = FT_New_Memory_Face(lib,(unsigned char*)data,dataSize,0,&face);
	if (error)
	{
		RadEngine.Debug.Warning("Cant create FreeType Face! FT_Error: "+std::string(FT_Extra_GetErrorMessage(error)));
		return false;
	}
	bool ret = LoadFromFace(file, lib, face, fontSize);
	FT_Done_Face(face);
	FT_Done_FreeType(lib);
	return ret;
}

void CResourceFont::Renew()
{
	Destroy();
	m_isDestroyed = false;
}

void CResourceFont::Destroy()
{
	if (!m_isDestroyed)
	{
		for (unsigned char c=0;c<255;c++)
		{
			if (m_character[c].textureID)
			{
				glDeleteTextures(1,&m_character[c].textureID);
				m_character[c].textureID = 0;
			}
		}
		m_isDestroyed = true;
	}
}

CResourceFont::Character CResourceFont::GetCharacter(unsigned char character)
{
	if (character<256)
	{
		return m_character[character];
	}
	CResourceFont::Character tmp;
	return tmp;
}

void CResourceFont::DrawChar(float x, float y, char character, ColorC color, float scale)
{
	RadEngine.Debug.Warning("Not implemented: CResourceFont::DrawChar");
}

void CResourceFont::DrawString(float x, float y, std::string str, ColorC color, float scale)
{
	if (str.size()==0||scale==0.0f)
		return;

	float xLast = x;
	for (unsigned int i=0;i<str.size();i++)
	{
		unsigned char c = static_cast<unsigned char>(str.at(i));
		if (c==10)
		{
			y+=m_height;
			x = xLast;
		}
		else
		{
			CResourceFont::Character *ch = &this->m_character[c];
			float xpos = x + ch->bearing.x * scale;
			float ypos = y - (ch->bearing.y) * scale;
			float w = ch->size.x * scale;
			float h = ch->size.y * scale;
			glLoadIdentity();

		   	glTranslatef(xpos,ypos,0.f);

		   	glEnable(GL_BLEND);
		   	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		   	glBindTexture(GL_TEXTURE_2D, ch->textureID);
		   	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);
		   	glColor3f((float)(color.r)/255.0f,(float)(color.g)/255.0f,(float)(color.b)/255.0f);

		   	glBegin( GL_QUADS );
		   		glTexCoord2f( 0, 0 ); glVertex2f( 0.f, 0.f );
		   		glTexCoord2f( 1.0f, 0 ); glVertex2f( w, 0.f );
		   		glTexCoord2f(1.0f,1.0f ); glVertex2f( w, h );
		   		glTexCoord2f( 0, 1.0f); glVertex2f( 0.f, h );
		   	glEnd();

		   	x += ((unsigned int)ch->advance.x>>6) * scale;
		}
	}
}

}
}
