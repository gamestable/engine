/*
 * Sound.cpp
 *
 *  Created on: Apr 6, 2017
 *      Author: Steffen Kremer
 */

#include "Sound.hpp"

namespace rad
{
namespace engine
{

bool CResourceSound::LoadFromWAVData(std::string name, void * data, unsigned int dataSize)
{
	return true;
}

bool CResourceSound::LoadFromWAV(std::string)
{
	return true;
}

void CResourceSound::Renew()
{
	Destroy();
	m_isDestroyed = false;
}

void CResourceSound::Destroy()
{

}

}
}
