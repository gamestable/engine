/*
 * Font.hpp
 *
 *  Created on: Feb 7, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MANAGER_RESOURCE_FONT_HPP_
#define RADENGINE_MANAGER_RESOURCE_FONT_HPP_

#include <iostream>
#include <vector>
#include "RadEngine/Extern/IncludeFT.hpp"
#include "RadEngine/DataTypes/Datatypes.hpp"
#include "RadEngine/Datatypes/Index.hpp"

namespace rad
{
namespace engine
{

class CResourceFont : private CIndexBase
{
public:
	CResourceFont();
	~CResourceFont();

	bool LoadFromFace(std::string file, FT_Library lib, FT_Face face, unsigned int fontSize = 24);

	bool LoadFromFile(std::string file, unsigned int fontSize = 24);
	bool LoadFromMemory(std::string file, unsigned int fontSize, void *data, unsigned int dataSize);

	struct Character
	{
		unsigned int textureID = 0;
		Vector2<unsigned int> size;
		Vector2<int> bearing;
		Vector2<int> advance;
	};
	Character GetCharacter(unsigned char character);

	void Renew();
	void Destroy();

	void DrawChar(float x, float y, char character, ColorC color, float scale = 1.0f);
	void DrawString(float x, float y, std::string, ColorC color, float scale = 1.0f);

private:
	Character m_character[256];
	std::string m_file;
	unsigned int m_height = 24;

	bool m_isDestroyed = true;
};

}
}

#endif /* RADENGINE_MANAGER_RESOURCE_FONT_HPP_ */
