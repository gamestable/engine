/*
 * Texture.hpp
 *
 *  Created on: Jan 31, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MANAGER_RESOURCE_TEXTURE_HPP_
#define RADENGINE_MANAGER_RESOURCE_TEXTURE_HPP_

#include "RadEngine/Datatypes/Datatypes.hpp"
#include "RadEngine/Datatypes/Index.hpp"

namespace rad
{
namespace engine
{

class CResourceTexture : private CIndexBase
{
public:
	CResourceTexture();
	~CResourceTexture();

	bool LoadCupboard();

	bool LoadFromPixels32(unsigned int* pixels, unsigned int imgWidth, unsigned int imgHeight, unsigned int texWidth, unsigned int texHeight );
	bool LoadFromImageData(std::string name, void * data, unsigned int dataSize, const std::string& confFileContent = "");

	bool LoadFromFile(std::string file, std::string confFile = "");

	void Renew();
	void Destroy();

	void Draw(Vector2<float> position, bool screen = false);
	void DrawExt(Vector2<float> position, Rect<float> *clip, Vector2<int> size, double rotation, ColorC color, bool screen = false);
	void DrawExt(Vector2<float> position, Rect<float> *clip, Vector2<float> size, double rotation, ColorC color, bool screen = false);

	void SetAlphaMask(ColorC color);

	bool IsLoaded() {return (m_id>0);}
	bool IsText() {return m_isText;}

	Vector2 <int>offset;
	double rotation = 0.0;

	std::string GetName() {return m_name;}
	unsigned int GetHash() {return m_nameHash;}
	Index GetIndex() {return m_index;}

	unsigned int GetGLID() {return m_id;}

private:
	void m_DrawExtSize(float sizeX, float sizeY);
	void m_DrawExtSize(unsigned int sizeX, unsigned int sizeY);
	void m_DrawExtRotation(int rotation);

	std::string m_file = "";
	bool m_isText = false;
	bool m_isSpriteSheet = false;

	unsigned int m_id = 0;
	unsigned int m_width = 0;
	unsigned int m_height = 0;
	unsigned int m_imageWidth = 0;
	unsigned int m_imageHeight = 0;

	ColorC m_color;
	bool m_alphaMask = false;

	bool m_isDestroyed = true;
};

}
}

#endif /* RADENGINE_MANAGER_RESOURCE_TEXTURE_HPP_ */
