/*
 * Resource.cpp
 *
 *  Created on: Feb 14, 2017
 *      Author: Steffen Kremer
 */

#include "Resource.hpp"
#include "../../RadEngine.hpp"

namespace rad
{
namespace engine
{

bool CResource::CSoundManager::Init()
{
	ALCenum error;
	m_alDevice = alcOpenDevice(nullptr);
	error = alGetError();
	if (error!=AL_NO_ERROR)
	{
		RadEngine.Debug.Error("Can't open ALDevice! Code: "+std::to_string(error));
		return false;
	}
	m_alContextGUI = alcCreateContext(m_alDevice, nullptr);
	error = alGetError();
	if (error!=AL_NO_ERROR)
	{
		RadEngine.Debug.Error("Can't create GUI ALContext! Code: "+std::to_string(error));
		return false;
	}
	m_alContext = alcCreateContext(m_alDevice, nullptr);
	error = alGetError();
	if (error!=AL_NO_ERROR)
	{
		RadEngine.Debug.Error("Can't create ALContext! Code: "+std::to_string(error));
		return false;
	}
	if (!alcMakeContextCurrent(m_alContextGUI))
	{
		error = alGetError();
		RadEngine.Debug.Error("Can't make ALContext current! Code: "+std::to_string(error));
		return false;
	}
	return true;
}

void CResource::CSoundManager::Destroy()
{
	m_isDestructing = true;
	for (unsigned int i=0;i<m_sound.size();i++)
		if (m_sound.at(i)!=nullptr)
			delete m_sound.at(i);
	alcMakeContextCurrent(NULL);
	alcDestroyContext(m_alContext);
	alcDestroyContext(m_alContextGUI);
	alcCloseDevice(m_alDevice);
	RadEngine.Debug.Log("Closing Audio...");
}

}
}
