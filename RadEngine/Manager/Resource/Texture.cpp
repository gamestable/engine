/*
 * Texture.cpp
 *
 *  Created on: Jan 31, 2017
 *      Author: Steffen Kremer
 */

#include "Texture.hpp"
#include "RadEngine/Extern/IncludeGL.hpp"
#include "RadEngine/RadEngine.hpp"

namespace rad
{
namespace engine
{

CResourceTexture::CResourceTexture()
{
	m_index = RadEngine.Resource.Texture.Add(this);
}

CResourceTexture::~CResourceTexture()
{
	Destroy();
	RadEngine.Resource.Texture.Remove(m_index);
}

bool CResourceTexture::LoadCupboard()
{
	const int CHECKERBOARD_WIDTH = 128;
	const int CHECKERBOARD_HEIGHT = 128;
	const int CHECKERBOARD_PIXEL_COUNT = CHECKERBOARD_WIDTH * CHECKERBOARD_HEIGHT;
	GLuint checkerBoard[CHECKERBOARD_PIXEL_COUNT];
	for( int i = 0; i < CHECKERBOARD_PIXEL_COUNT; ++i )
	{
		GLubyte* colors = (GLubyte*)&checkerBoard[ i ];
		if( (i / 128 & 16) ^ (i % 128 & 16) )
		{
			colors[ 0 ] = 0xFF;
			colors[ 1 ] = 0xFF;
			colors[ 2 ] = 0xFF;
			colors[ 3 ] = 0xFF;
		}
		else
		{
			colors[ 0 ] = 0xFF;
			colors[ 1 ] = 0x00;
			colors[ 2 ] = 0x00;
			colors[ 3 ] = 0xFF;
		}
	}
	return LoadFromPixels32(checkerBoard,CHECKERBOARD_WIDTH, CHECKERBOARD_HEIGHT,CHECKERBOARD_WIDTH,CHECKERBOARD_HEIGHT);
}

bool CResourceTexture::LoadFromPixels32(unsigned int* pixels, unsigned int imgWidth, unsigned int imgHeight, unsigned int texWidth, unsigned int texHeight)
{
	Renew();
	if (pixels==nullptr)
	{
		RadEngine.Debug.Warning("Missing Parameters to LoadFromPixels32!");
		return false;
	}
	m_width = texWidth;
	m_height = texHeight;
	m_imageWidth = imgWidth;
	m_imageHeight = imgHeight;
	if (m_alphaMask)
		{
			GLuint size = texHeight * texWidth;
			for( unsigned int i = 0; i < size; ++i )
			{
				//Get pixel colors
				GLubyte* colors = (GLubyte*)&pixels[ i ];
				//Color matches
				if( colors[ 0 ] == m_color.r && colors[ 1 ] == m_color.g && colors[ 2 ] == m_color.b && ( 0 == m_color.a || colors[ 3 ] == m_color.a ) )
				{
					//Make transparent
					colors[ 0 ] = 255;
					colors[ 1 ] = 255;
					colors[ 2 ] = 255;
					colors[ 3 ] = 000;
				}
			}
		}
		//Generate texture ID
		glGenTextures(1,&m_id);
		GLenum error = glGetError();
		if(error!=GL_NO_ERROR)
		{
			RadEngine.Debug.Warning("openGL Error LoadFromPixels32 "+std::to_string(error));
			return false;
		}
		//Bind texture ID
		glBindTexture(GL_TEXTURE_2D,m_id);
		//Generate texture
		error = glGetError();
		if(error!=GL_NO_ERROR)
		{
			RadEngine.Debug.Warning("openGL Error 1 LoadFromPixels32 "+std::to_string(error));
			return false;
		}
		glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,texWidth,texHeight,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
		error = glGetError();
		if(error!=GL_NO_ERROR)
		{
			RadEngine.Debug.Warning("openGL Error 2 LoadFromPixels32 "+std::to_string(error));
			return false;
		}
		//Set texture parameters
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		//Unbind texture
		glBindTexture(GL_TEXTURE_2D,0);
		//Check for error
		error = glGetError();
		if(error!=GL_NO_ERROR)
		{
			RadEngine.Debug.Warning("openGL Error 3 LoadFromPixels32 "+std::to_string(error));
			return false;
		}
		//m_textFont = nof_index;
		return true;
}

unsigned int GetILenumFromExtension(std::string extension)
{
	if(extension=="bmp")
	{
		return IL_BMP;
	}
	else if (extension=="ico")
	{
		return IL_ICO;
	}
	else if (extension=="jpg")
	{
		return IL_JPG;
	}
	else if (extension=="png")
	{
		return IL_PNG;
	}
	else if (extension=="tga")
	{
		return IL_TGA;
	}
	else if (extension=="tif")
	{
		return IL_TIF;
	}
	else if (extension=="raw")
	{
		return IL_RAW;
	}
	else if (extension=="gif")
	{
		return IL_GIF;
	}
	else if (extension=="dds")
	{
		return IL_DDS;
	}
	else if (extension=="iff")
	{
		return IL_IFF;
	}
	return IL_TYPE_UNKNOWN;
}

bool CResourceTexture::LoadFromImageData(std::string name, void * data, unsigned int dataSize,  const std::string& confFileContent)
{
	Renew();
	if (name==""||data==nullptr||dataSize==0)
	{
		RadEngine.Debug.Warning("Missing Parameters to loadImageData!");
		return false;
	}
	ILuint imgID = 0;
	ilGenImages(1,&imgID);
	ilBindImage(imgID);
	if (ilLoadL(GetILenumFromExtension(RadEngine.File.GetExtension(name)),data,dataSize)!=IL_TRUE)
	{
		RadEngine.Debug.Warning("Cant load "+name+" from "+std::to_string((unsigned int)data)+" with DevIL from: "+std::to_string((unsigned int)data));
		return false;
	}
	if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE)!=IL_TRUE)
	{
		RadEngine.Debug.Warning("Cant convert "+name+"from "+std::to_string((unsigned int)data)+" with DevIL!");
		return false;
	}
	GLuint imgWidth=(GLuint)ilGetInteger(IL_IMAGE_WIDTH);
	GLuint imgHeight=(GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
	GLuint texWidth=RadEngine.Math.PowerOfTwo(imgWidth);
	GLuint texHeight=RadEngine.Math.PowerOfTwo(imgHeight);
	if(imgWidth!=texWidth||imgHeight!=texHeight)
	{
		//Place image at upper left
		iluImageParameter(ILU_PLACEMENT,ILU_UPPER_LEFT);
		//Resize image
		iluEnlargeCanvas((int)texWidth,(int)texHeight,1);
	}
	if(!LoadFromPixels32((GLuint*)ilGetData(),imgWidth,imgHeight,texWidth,texHeight))
	{
		RadEngine.Debug.Warning("Cant load Pixel32 Data!");
		return false;
	}
	this->m_file=name;
	this->m_isText=false;
	this->m_name=RadEngine.File.FileNameToName(name);
	this->m_nameHash = HSID(m_name.c_str());
	return true;
}

bool CResourceTexture::LoadFromFile(std::string file, std::string confFile)
{
	if (file=="")
		return false;
	Renew();
	ILuint imgID = 0;
	ilGenImages(1,&imgID);
	ilBindImage(imgID);
	if (ilLoadImage(file.c_str())!=IL_TRUE)
	{
		RadEngine.Debug.Warning("Cant open "+file+" with DevIL!");
		return false;
	}
	if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE)!=IL_TRUE)
	{
		RadEngine.Debug.Warning("Cant convert "+file+" with DevIL!");
		return false;
	}
	GLuint imgWidth=(GLuint)ilGetInteger(IL_IMAGE_WIDTH);
	GLuint imgHeight=(GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
	GLuint texWidth=RadEngine.Math.PowerOfTwo(imgWidth);
	GLuint texHeight=RadEngine.Math.PowerOfTwo(imgHeight);
	if(imgWidth!=texWidth||imgHeight!=texHeight)
	{
		//Place image at upper left
		iluImageParameter(ILU_PLACEMENT,ILU_UPPER_LEFT);
		//Resize image
		iluEnlargeCanvas((int)texWidth,(int)texHeight,1);
	}
	if(!LoadFromPixels32((GLuint*)ilGetData(),imgWidth,imgHeight,texWidth,texHeight))
	{
		return false;
	}
	this->m_file=file;
	this->m_isText=false;
	this->m_name=RadEngine.File.FileNameToName(file);
	this->m_nameHash = HSID(m_name.c_str());
	return true;
}

void CResourceTexture::Renew()
{
	Destroy();
	m_isDestroyed = false;
}

void CResourceTexture::Destroy()
{
	if (!m_isDestroyed)
	{
		if(this->m_id!=0)
		{
			glDeleteTextures(1,&m_id);
			m_id = 0;
		}
		m_width = 0;
		m_height = 0;
		m_imageWidth = 0;
		m_imageHeight = 0;
		m_isDestroyed = true;
	}
}

void CResourceTexture::Draw(Vector2<float> position, bool screen)
{
	DrawExt(position, nullptr, Vector2<float>(1,1), 0, ColorC::All, screen);
}

void CResourceTexture::DrawExt(Vector2<float> position, Rect<float> *clip, Vector2<int> size, double rotation, ColorC color, bool screen)
{
	DrawExt(position, nullptr, Vector2<float>((float)size.x/(float)m_imageWidth, (float)size.y/(float)m_imageHeight), rotation, color, screen);
}

void CResourceTexture::DrawExt(Vector2<float> position, Rect<float> *clip, Vector2<float> size, double rotation, ColorC color, bool screen)
{
	if (m_id==0)
		return;
	if (screen)
		RadEngine.Display.Camera.SwitchCameraMode();
	glLoadIdentity();
	float texTop = 0.f;
	float texBottom = (float)m_imageHeight / (float)m_height;
	float texLeft = 0.f;
	float texRight = (float)m_imageWidth / (float)m_width;

	float quadWidth = m_imageWidth;
	float quadHeight = m_imageHeight;

	if (clip!=nullptr)
	{
		//Texture coordinates
		texLeft = clip->x / m_width;
		texRight = ( clip->x + clip->w ) / m_width;
		texTop = clip->y / m_height;
		texBottom = ( clip->y + clip->h ) / m_height;
		//Vertex coordinates
		quadWidth = clip->w;
		quadHeight = clip->h;
	}
	glTranslatef(position.x-offset.x*size.x,position.y-offset.y*size.y,0.f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef((m_imageWidth*size.x)/2, (m_imageHeight*size.y)/2, 0);
	glRotatef(rotation,0.f,0.f,1.f);
	glTranslatef(-(m_imageWidth*size.x)/2, -(m_imageHeight*size.y)/2, 0);
	glScalef(size.x,size.y,0.f);
	//glColor4f(1.0f,1.0f,1.0f,0.f);
	glBindTexture(GL_TEXTURE_2D,m_id);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	//glTexEnvf(GL_TEXTURE_2D,GL_TEXTURE_ENV_MODE,GL_MODULATE); //Causes Error 1280!
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(color.r/255.0f,color.g/255.0f,color.b/255.0f,color.a/255.0f);
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	//Render textured quad
	glBegin( GL_QUADS );
		glTexCoord2f( texLeft, texTop ); glVertex2f( 0.f, 0.f );
		glTexCoord2f( texRight, texTop ); glVertex2f( quadWidth, 0.f );
		glTexCoord2f( texRight, texBottom ); glVertex2f( quadWidth, quadHeight );
		glTexCoord2f( texLeft, texBottom ); glVertex2f( 0.f, quadHeight );
	glEnd();
	glDisable(GL_BLEND);
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
	GLenum error = glGetError();
	if(error!=GL_NO_ERROR)
	{
		RadEngine.Debug.Warning("Draw Texture Error! "+std::to_string(error));
	}
	if (screen)
		RadEngine.Display.Camera.SwitchCameraMode();
}

void CResourceTexture::SetAlphaMask(ColorC color)
{
	m_color = color;
}

}
}
