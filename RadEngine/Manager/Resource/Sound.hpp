/*
 * Sound.hpp
 *
 *  Created on: Feb 10, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_MANAGER_RESOURCE_SOUND_HPP_
#define RADENGINE_MANAGER_RESOURCE_SOUND_HPP_

#include <iostream>
#include <vector>
//#include "RadEngine/DataTypes/Datatypes.hpp"
#include "RadEngine/Datatypes/Index.hpp"
#include "RadEngine/Extern/IncludeAL.hpp"

namespace rad
{
namespace engine
{

class CResourceSound : private CIndexBase
{
public:
	CResourceSound() {};
	~CResourceSound() {};

	bool LoadFromWAVData(std::string name, void * data, unsigned int dataSize);
	bool LoadFromWAV(std::string file);

	void Renew();
	void Destroy();

	bool IsLoaded() {return (m_id>0);}

	std::string GetName() {return m_name;}
	unsigned int GetHash() {return m_nameHash;}
	Index GetIndex() {return m_index;}

private:
	std::string m_file = "";

	unsigned int m_id = 0;

	bool m_isDestroyed = true;
};

}
}

#endif /* RADENGINE_MANAGER_RESOURCE_SOUND_HPP_ */
