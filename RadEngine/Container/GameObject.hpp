/*
 * GameObject.hpp
 *
 *  Created on: Jan 31, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_GAMEOBJECT_HPP_
#define RADENGINE_CONTAINER_GAMEOBJECT_HPP_

#include "RadEngine/Datatypes/Index.hpp"
#include "Components/IncludeComponents.hpp"
#include "GraphNode.hpp"

namespace rad
{
namespace engine
{

class CGameState;

class CGameObject : public CIndexBase
{
public:
	CGameObject(std::string name, CGameState* state, CGameObject *self, CGameObject *parent, bool heap = false);
	virtual ~CGameObject();

	virtual bool Init() = 0;
	virtual void Destroy() = 0;

	virtual void HandleEvent() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;

	/*template <typename Type>
	void AddComponent();
	template <typename Type>
	void RemoveComponent();
	template <typename Type>
	Type* GetComponent();
	template <typename Type>
	Type* GetComponent(short int id);*/

	std::string GetName() {return m_name;}
	unsigned int GetHash() {return m_nameHash;}
	Index GetIndex() {return m_index;}
	CGameState* GetState() {return m_state;}
	unsigned int GetParent() {return m_parentObjectIndex;}
	CGraphNode<CGameObjectEntry>* GetSceneNode();
	bool IsInHeap() {return m_heap;}

	CTransform transform;

protected:
	Index m_index = NOF_INDEX;
	std::string m_name = "";
	unsigned int m_nameHash = 0;
	CGraphNode<CGameObjectEntry>* m_sceneNode = nullptr;

	unsigned int m_parentObjectIndex = 0;
	CGameState *m_state = nullptr;
	CGameObject *m_self = nullptr;
	bool m_heap = false;
};

}
}

#endif /* RADENGINE_CONTAINER_GAMEOBJECT_HPP_ */
