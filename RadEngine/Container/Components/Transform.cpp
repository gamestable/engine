/*
 * Transform.cpp
 *
 *  Created on: Mar 8, 2017
 *      Author: Steffen Kremer
 */

#include "Transform.hpp"
#include "../GameObject.hpp"
#include <cmath>
#include "math.h"
#include "RadEngine/RadEngine.hpp"

namespace rad
{
namespace engine
{

void CTransform::Update()
{
	if (m_isInComponent)
	{
		CGameObject *tmp = GetGameObject();
		if (tmp!=nullptr)
		{
			m_scale.x = tmp->transform.GetGlobalScale().x * localScale.x;
			m_scale.y = tmp->transform.GetGlobalScale().y * localScale.y;

			float angle = Math->Deg2Rad(tmp->transform.GetGlobalRotation());
			m_position.x = tmp->transform.GetGlobalPosition().x + (cos(angle) * localPosition.x * m_scale.x) + (-sin(angle) * localPosition.y * m_scale.y);
			m_position.y = tmp->transform.GetGlobalPosition().y + (sin(angle) * localPosition.x * m_scale.x) + (cos(angle) * localPosition.y * m_scale.y);

			m_rotation = tmp->transform.GetGlobalRotation() + localRotation;
			return;
		}
	}
	else
	{
		unsigned int tmpI = GetGameObject()->GetParent();
		if (tmpI!=0)
		{
			CGameObject *tmp = GetGameObject()->GetState()->GetObject(tmpI);
			if (tmp!=nullptr)
			{
				m_scale.x = tmp->transform.GetGlobalScale().x * localScale.x;
				m_scale.y = tmp->transform.GetGlobalScale().y * localScale.y;

				float angle = Math->Deg2Rad(tmp->transform.GetGlobalRotation());
				m_position.x = tmp->transform.GetGlobalPosition().x + (cos(angle) * localPosition.x * m_scale.x) + (-sin(angle) * localPosition.y * m_scale.y);
				m_position.y = tmp->transform.GetGlobalPosition().y + (sin(angle) * localPosition.x * m_scale.x) + (cos(angle) * localPosition.y * m_scale.y);

				m_rotation = tmp->transform.GetGlobalRotation() + localRotation;
				return;
			}
		}
	}
	m_position.x = localPosition.x;
	m_position.y = localPosition.y;
	m_rotation = localRotation;
	m_scale.x = localScale.x;
	m_scale.y = localScale.y;
}

void CTransform::LookAt(CGameObject *object, float offset)
{
	LookAt(object->transform.GetGlobalPosition(), offset);
}

inline float fmod(float x, float y)
{
	return x-(std::trunc(x/y)*y);
}

inline float fixDir(double x)
{
	return fmod(x+360, 360);
}

void CTransform::LookAt(Vector2<float>pos, float offset)
{
	Update();
	localRotation = -(360-fixDir(atan2(m_position.y-pos.y,m_position.x-pos.x)*(180/Math->pi)))+offset; //localR instead m_rot
}

void CTransform::RotateAround(Vector2<float> offset, float angle)
{
	float s = sin(angle);
	float c = cos(angle);

	offset.x -= m_position.x;
	offset.y -= m_position.y;

	float xnew = offset.x * c - offset.y * s;
	float ynew = offset.x * s + offset.y * c;

	offset.x = xnew + m_position.x;
	offset.y = ynew + m_position.y;
}

}
}
