/*
 * Transform.hpp
 *
 *  Created on: Mar 8, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_TRANSFORM_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_TRANSFORM_HPP_

#include "../Component.hpp"
#include "RadEngine/Datatypes/DataTypes.hpp"

namespace rad
{
namespace engine
{

class CGameObject;

class CTransform : public CComponent
{
public:
	CTransform(CGameObject *gameObject, bool isInComponent = false) : CComponent(gameObject), localScale(1.0f,1.0f), m_isInComponent(isInComponent) {}
	virtual ~CTransform() {}

	void Update();
	void Draw() {}

	Vector2<float> GetGlobalPosition() {return m_position;}
	float GetGlobalRotation() {return m_rotation;}
	Vector2<float> GetGlobalScale() {return m_scale;}

	void LookAt(CGameObject *object, float offset = 0.0f);
	void LookAt(Vector2<float> position, float offset = 0.0f);

	void RotateAround(Vector2<float> offset, float angle);

	Vector2<float> localPosition;
	float localRotation = 0.0f;
	Vector2<float> localScale;

private:
	Vector2<float> m_position;
	float m_rotation = 0.0f;
	Vector2<float> m_scale;
	bool m_isInComponent = false;
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_TRANSFORM_HPP_ */
