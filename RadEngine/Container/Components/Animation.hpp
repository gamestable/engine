/*
 * Animation.hpp
 *
 *  Created on: May 11, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_ANIMATION_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_ANIMATION_HPP_

#include "../Component.hpp"
#include "Transform.hpp"
#include "RadEngine/Datatypes/DataTypes.hpp"

namespace rad
{
namespace engine
{

class CAnimation : public CComponent
{
public:
	CAnimation(CGameObject *gameObject) : CComponent(gameObject), transform(gameObject, true) {}
	virtual ~CAnimation() {}

	virtual bool Init() {return true;};
	virtual void Destroy() {};

	//virtual void HandleEvent() = 0;
	virtual void Update() {};
	virtual void Draw() {};

	void Pause(bool pause) {m_pause = true;}
	void Resume(bool pause) {m_pause = false;}
	bool IsPaused() {return m_pause;}

	CTransform transform;

private:
	bool m_pause = false;
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_ANIMATION_HPP_ */
