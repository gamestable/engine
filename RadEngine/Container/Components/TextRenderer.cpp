/*
 * TextRenderer.cpp
 *
 *  Created on: May 22, 2017
 *      Author: Steffen Kremer
 */

#include "RadEngine/RadEngine.hpp"
#include "TextRenderer.hpp"

namespace rad
{
namespace engine
{

void CTextRenderer::Update()
{
	transform.Update();
}

void CTextRenderer::Draw()
{
	if (fontIndex!=0)
	{
		Resource->Font.Get(fontIndex)->DrawString(transform.GetGlobalPosition().x, transform.GetGlobalPosition().y, text, color.ToColorChar(), transform.GetGlobalScale().x);
	}
}

}
}
