/*
 * SpriteAnimation.hpp
 *
 *  Created on: May 11, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_SPRITEANIMATION_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_SPRITEANIMATION_HPP_

#include "Animation.hpp"

namespace rad
{
namespace engine
{

class CGameObject;

class CSpriteAnimation : public CAnimation
{
public:
	CSpriteAnimation(CGameObject *gameObject) : CAnimation(gameObject) {}
	virtual ~CSpriteAnimation() {}

	virtual bool Init(unsigned int textureID) {return true;};
	virtual void Destroy() {};

	virtual void Update() {};
	virtual void Draw() {};

private:
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_SPRITEANIMATION_HPP_ */
