/*
 * AudioListener.hpp
 *
 *  Created on: Apr 6, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_AUDIOLISTENER_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_AUDIOLISTENER_HPP_

#include "../Component.hpp"
#include "Transform.hpp"
#include "RadEngine/Datatypes/DataTypes.hpp"

namespace rad
{
namespace engine
{

class CAudioListener : public CComponent
{
public:
	CAudioListener(CGameObject *gameObject) : CComponent(gameObject), transform(gameObject, true) {}
	virtual ~CAudioListener() {}

	virtual bool Init() {return true;}
	virtual void Destroy() {}

	//virtual void HandleEvent() = 0;
	virtual void Update();
	virtual void Draw() {};

	void SetVolume(float vol);
	float GetVolume() {return m_volume;}

	void SetPause(bool pause);
	bool GetPause() {return m_pause;}

	CTransform transform;

private:
	bool m_pause = false;
	float m_volume = 1.0f;
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_AUDIOLISTENER_HPP_ */
