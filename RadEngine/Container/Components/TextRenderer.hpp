/*
 * TextRenderer.hpp
 *
 *  Created on: May 22, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_TEXTRENDERER_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_TEXTRENDERER_HPP_

#include "Renderer.hpp"

namespace rad
{
namespace engine
{

class CGameObject;

class CTextRenderer : public CRenderer
{

public:
	CTextRenderer(CGameObject *gameObject) : CRenderer(gameObject) {}
	virtual ~CTextRenderer() {}

	virtual bool Init(unsigned int fontID, unsigned int fontSize) {this->fontIndex = fontID; this->fontSize = fontSize; return true;}
	virtual void Destroy() {};

	virtual void Update();
	virtual void Draw();

	virtual bool IsOnScreen() {return false;}

	unsigned int fontIndex = 0;
	unsigned int fontSize = 0;

	std::string text = "Sample Text";
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_TEXTRENDERER_HPP_ */
