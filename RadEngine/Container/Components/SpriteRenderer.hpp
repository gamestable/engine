/*
 * SpriteRenderer.hpp
 *
 *  Created on: Mar 15, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_SPRITERENDERER_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_SPRITERENDERER_HPP_

#include "Renderer.hpp"

namespace rad
{
namespace engine
{

class CGameObject;

class CSpriteRenderer : public CRenderer
{
public:
	CSpriteRenderer(CGameObject *gameObject) : CRenderer(gameObject) {}
	virtual ~CSpriteRenderer() {}

	virtual bool Init(unsigned int textureID) {this->textureIndex = textureID; return true;}
	virtual void Destroy() {};

	virtual void Update();
	virtual void Draw();

	virtual bool IsOnScreen() {return false;}

	unsigned int textureIndex = 0;

private:
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_SPRITERENDERER_HPP_ */
