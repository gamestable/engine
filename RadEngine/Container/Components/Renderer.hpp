/*
 * Renderer.hpp
 *
 *  Created on: Mar 15, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENTS_RENDERER_HPP_
#define RADENGINE_CONTAINER_COMPONENTS_RENDERER_HPP_

#include "Transform.hpp"

namespace rad
{
namespace engine
{

class CRenderer : public CComponent
{
public:
	CRenderer(CGameObject *gameObject) : CComponent(gameObject), transform(gameObject, true), color(1.0f, 1.0f, 1.0f, 1.0f) {}
	virtual ~CRenderer() {}

	virtual bool Init() {return true;}
	virtual void Destroy() {}

	//virtual void HandleEvent() = 0;
	virtual void Update() {};
	virtual void Draw() {};

	virtual bool IsOnScreen() = 0;

	CTransform transform;

	bool isVisible = true;
	ColorF color;
	bool flipX = false;
	bool flipY = false;
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENTS_RENDERER_HPP_ */
