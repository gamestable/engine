/*
 * AudioListener.cpp
 *
 *  Created on: Apr 6, 2017
 *      Author: Steffen Kremer
 */

#include "AudioListener.hpp"
#include "RadEngine/Extern/IncludeAL.hpp"

namespace rad
{
namespace engine
{

void CAudioListener::Update()
{
	alListener3f(AL_POSITION, transform.GetGlobalPosition().x, transform.GetGlobalPosition().y, 0.0f);
	alListener3f(AL_VELOCITY, 0, 0, 0);
}

void CAudioListener::SetPause(bool pause)
{

}

void CAudioListener::SetVolume(float vol)
{
	if (vol>=0.0f&&vol<=1.0f)
	{
		m_volume = vol;
		alListenerf(AL_GAIN, vol);
	}
}

}
}
