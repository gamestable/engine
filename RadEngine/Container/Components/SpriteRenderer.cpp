/*
 * SpriteRenderer.cpp
 *
 *  Created on: Mar 15, 2017
 *      Author: Steffen Kremer
 */

#include "RadEngine/RadEngine.hpp"
#include "SpriteRenderer.hpp"

namespace rad
{
namespace engine
{

void CSpriteRenderer::Update()
{
	transform.Update();
}

void CSpriteRenderer::Draw()
{
	if (textureIndex!=0)
	{
		Resource->Texture.Get(textureIndex)->DrawExt(transform.GetGlobalPosition(), nullptr, transform.GetGlobalScale(), transform.GetGlobalRotation(), color.ToColorChar(), false);
	}
}

}
}
