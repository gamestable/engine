/*
 * GraphNode.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: Steffen Kremer
 */

#include "GraphNode.hpp"
#include "RadEngine/RadEngine.hpp"
#include "RadEngine/Datatypes/Index.hpp"

namespace rad
{
namespace engine
{

template <typename T>
CGraphNode<T>::CGraphNode(std::string name)
{
	m_nameHash = HSID(name.c_str());
	m_name = name;
}

template <typename T>
CGraphNode<T>::CGraphNode(std::string name, CGraphNode<T> *topNode, T value, std::vector<CGraphNode<T> *> *subNode)
{
	m_nameHash = HSID(name.c_str());
	m_name = name;
	m_value = value;
	m_topNode = topNode;
	if (subNode!=nullptr)
	{
		for (unsigned int i=0;i<subNode->size();i++)
		{
			m_subNodes.push_back(subNode->at(i));
		}
	}
}

template <typename T>
CGraphNode<T>::~CGraphNode()
{
	//std::cout << "DECON NODE: " << GetName() << std::endl;
	//std::flush(std::cout);
}

template <typename T>
CGraphNode<T>* CGraphNode<T>::GetRoot()
{
	if (IsRoot())
	{
		return this;
	}
	std::stack<CGraphNode<T>*> itStack;
	itStack.push(this);
	while (!itStack.empty())
	{
		CGraphNode<T>* tmpNode = itStack.top();
		itStack.pop();
		if (tmpNode->IsRoot())
		{
			return tmpNode;
		}
		else
		{
			itStack.push(tmpNode->GetTop());
		}
	}
	Engine->Debug.Error("Cant find root of Graph! This should never happen every tree has a root!");
	return nullptr; //This should never happen every tree has a root
}

template <typename T>
CGraphNode<T> * CGraphNode<T>::NewSub(std::string name, CGraphNode<T> *topNode, T value, std::vector<CGraphNode<T> *> *subNode)
{
	CGraphNode<T> * node = new CGraphNode<T>(name, topNode, value, subNode);
	m_subNodes.push_back(node);
	return node;
}

template <typename T>
unsigned int CGraphNode<T>::CountBelow() //Count all subnodes below this Node
{
	unsigned int nodesBelow = 0;
	std::stack<CGraphNode<T>*> itStack;
	itStack.push(this);
	while (!itStack.empty())
	{
		CGraphNode<T>* tmpNode = itStack.top();
		itStack.pop();
		if (tmpNode->CountSubs())
		{
			for (unsigned int i=tmpNode->CountSubs();i>0;--i)
			{
				itStack.push(tmpNode->GetSub(i-1));
			}
		}
	}
	return nodesBelow;
}

template <typename T>
CGraphNode<T> *CGraphNode<T>::GetSub(unsigned int index)
{
	if (index<m_subNodes.size()&&m_subNodes.size()!=0)
	{
		return m_subNodes.at(index);
	}
	Engine->Debug.Warning("Cant get Subnode with index "+std::to_string(index)+" below "+m_name);
	return nullptr;
}

template <typename T>
CGraphNode<T> *CGraphNode<T>::Search(std::string name)
{
	std::stack<CGraphNode<T>*> itStack;
	itStack.push(this);
	while (!itStack.empty())
	{
		CGraphNode<T>* tmpNode = itStack.top();
		itStack.top();
		if (tmpNode->GetName()==name)
		{
			return tmpNode;
		}
		for (unsigned int i=tmpNode->CountSubs();i>0;--i)
		{
			itStack.push(tmpNode->GetSub(i));
		}
	}
	Engine->Debug.Warning("Cant find "+name+" in GraphNode!");
	return nullptr;
}

template <typename T>
void CGraphNode<T>::RemoveSub(CGraphNode<T> *sub)
{
	for (unsigned int i=0;i<m_subNodes.size();i++)
	{
		if (m_subNodes.at(i)==sub)
		{
			m_subNodes.erase(m_subNodes.begin() + i);
			break;
		}
	}
	//TODO: Make this code work! - https://en.wikipedia.org/wiki/Tree_traversal
	/*std::stack<CGraphNode*> itStack;
	CGraphNode<T>* node = this;
	CGraphNode<T>* lastNode = nullptr;
	CGraphNode<T>* tmpNode = nullptr;
	CGraphNode<T>* peekNode = nullptr;
	itStack.push(this);
	while (!itStack.empty()||node!=nullptr)
	{
		if (node!=nullptr)
		{
			for (unsigned int i=tmpNode->CountSubs();i>0;--i)
			{
				itStack.push(tmpNode->GetSub(i));
			}
		}
		else
		{
			peekNode = itStack.top();
			for (unsigned int i=tmpNode->CountSubs();i>0;--i)
			{
				itStack.push(tmpNode->GetSub(i));
			}
		}

		CGraphNode* tmpNode = itStack.pop();
		if (tmpNode->CountSubs()==0)
		{
			delete tmpNode;
		}
		else
		{

		}
	}*/
}

template <typename T>
void CGraphNode<T>::Move(CGraphNode<T> target)
{
	m_topNode->RemoveSub(this);
	target.AddSub(this);
}

template <typename T>
void CGraphNode<T>::DestroySubs() //Warning recursive!
{
	//std::cout << GetName() << " - " << m_subNodes.size() << std::endl;
	for (unsigned int i=0;i<m_subNodes.size();i++)
	{
		m_subNodes.at(i)->DestroySubs();
		delete m_subNodes.at(i);
	}
	m_subNodes.clear();
}

template <typename T>
void CGraphNode<T>::Destroy()
{
	//std::cout << "DESTROY NODE:" << GetName() << std::endl;
	DestroySubs();
	if (m_topNode!=nullptr)
	{
		m_topNode->RemoveSub(this);
	}
	//std::cout << "DESTROYED NODE:" << GetName() << std::endl;
	//delete this;
}

template class CGraphNode<CGameObjectEntry>;

}
}
