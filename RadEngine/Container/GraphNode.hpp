/*
 * GraphNode.hpp
 *
 *  Created on: Mar 3, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_GRAPHNODE_HPP_
#define RADENGINE_CONTAINER_GRAPHNODE_HPP_

#include <vector>
#include <string>
#include "RadEngine/Module/Math/Hash.hpp"

#include <iostream>
#include <string>
#include <regex>

namespace rad
{
namespace engine
{

//TODO: Add identifier like string or int=hash(string) for object identf

template <typename T>
class CGraphNode
{
public:
	CGraphNode(std::string name);
	CGraphNode(std::string name, CGraphNode<T> *topNode, T value, std::vector<CGraphNode<T> *> *subNode = nullptr);
	~CGraphNode();

	void SetValue(T value) {m_value = value;}
	T GetValue() {return m_value;}
	std::string GetName() {return m_name;}
	unsigned int GetHash() {return m_nameHash;}

	void SetRoot() {m_topNode=nullptr;}
	void SetTop(CGraphNode<T> *node) {m_topNode = node;}
	CGraphNode<T>* GetTop() {return m_topNode;}
	bool IsRoot() {return (m_topNode==nullptr);}
	CGraphNode<T>* GetRoot();

	void AddSub(CGraphNode<T> *node) {m_subNodes.push_back(node);}
	CGraphNode<T> * NewSub(std::string name, CGraphNode<T> *topNode, T value, std::vector<CGraphNode<T> *> *subNode = nullptr);
	unsigned int CountSubs() {return m_subNodes.size();}
	unsigned int CountBelow(); //Count all subnodes below this Node
	CGraphNode<T> *GetSub(unsigned int index);
	CGraphNode<T> *Search(std::string name);
	std::vector<CGraphNode<T>*> SearchSubs(); //Return array if find multiple identical objects (eg bullets)
	void RemoveSub(CGraphNode<T> *sub);
	void Move(CGraphNode<T> target);
	void ClearSubs() {m_subNodes.clear();} //Garbage!
	void DestroySubs(); //Warning recursive!
	void Destroy();

private:
	std::string m_name;
	unsigned int m_nameHash = 0;
	T m_value;
	CGraphNode *m_topNode = nullptr;
	std::vector<CGraphNode *> m_subNodes;
};

}
}

#endif /* RADENGINE_CONTAINER_GRAPHNODE_HPP_ */
