/*
 * GameState.hpp
 *
 *  Created on: 31.12.2016
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_GAMESTATE_HPP_
#define RADENGINE_CONTAINER_GAMESTATE_HPP_

#include <iostream>
#include <vector>
#include "GameObject.hpp"
#include "GraphNode.hpp"
#include "RadEngine/Extern/IncludeB2D.hpp"

namespace rad
{
namespace engine
{

class CGameState
{
public:
	CGameState(std::string name) : m_name(name), m_physicsGravity(0.0f, 9.81f), m_sceneGraph(name) {}
	virtual ~CGameState();

	virtual bool Init() = 0;
	bool InitObjects();
	virtual void Destroy() = 0;
	void DestroyObjects();

	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual void HandleEvent() = 0;
	bool HandleEventObjects();
	virtual void Update() = 0;
	bool UpdateObjects();
	virtual void Draw() = 0;
	bool DrawObjects();

	bool IsPopUp() {return m_popUp;}
	std::string GetName() {return m_name;}

	bool DrawUnderlying() {return m_drawUnderlying;}
	bool UpdateUnderlying() {return m_updateUnderlying;}

	CGraphNode<CGameObjectEntry>* AddObject(CGameObject *obj, CGameObject *parentObj);
	CGameObject* GetObject(unsigned int index);
	unsigned int FindObject(CGameObject *obj);
	CGameObject* FindObject(std::string name);
	CGameObject* FindObject(unsigned int nameHash);
	void RemoveObject(CGameObject *obj);
	void RemoveObject(unsigned int index);

	CGraphNode<CGameObjectEntry>* GetSceneGraph() {return &m_sceneGraph;}

	b2World *GetPhysicsWorld();
	Vector2<float> GetPhysicsGravity() {return m_physicsGravity;}
	void SetPhysicsGravity(float x, float y) {m_physicsGravity.Set(x,y);m_physicsWorld->SetGravity(b2Vec2(m_physicsGravity.x, m_physicsGravity.y));}

	enum STATE
	{
		STATE_TRANSITION_ON,
		STATE_TRANSITION_OFF,
		STATE_HIDDEN,
		STATE_ACTIVE
	};

private:
	std::string m_name;
	STATE m_state = STATE_ACTIVE;
	bool m_popUp = false;
	bool m_drawUnderlying = false;
	bool m_updateUnderlying = false;
	unsigned int transitionTimeOn = 0;
	unsigned int transitionTimeOff = 0;

	b2World *m_physicsWorld = nullptr;
	Vector2<float> m_physicsGravity;
	int m_physicsVelocityIterations = 6;
	int m_physicsPositionIterations = 2;

	unsigned int m_FindFreeIndex();

	std::vector<CGameObjectEntry> m_object; //replaced with graphnode and graph
	CGraphNode<CGameObjectEntry> m_sceneGraph; //TODO: Replace all vector stuff and use scenegraph
};

}
}

#endif /* RADENGINE_CONTAINER_GAMESTATE_HPP_ */
