/*
 * Component.hpp
 *
 *  Created on: Mar 8, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_CONTAINER_COMPONENT_HPP_
#define RADENGINE_CONTAINER_COMPONENT_HPP_


namespace rad
{
namespace engine
{

class CGameObject;

class CComponent
{
public:
	CComponent(CGameObject *gameObject)
	{
		m_gameObject = gameObject;
	}
	virtual ~CComponent() {}

	virtual bool Init() {return true;}
	virtual void Destroy() {}

	//virtual void HandleEvent() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;

	CGameObject *GetGameObject() {return m_gameObject;}
	//short int GetID() {return m_id;}

	bool enabled = true;
private:
	CGameObject * m_gameObject = nullptr;
	//short int m_id = 0;
};

}
}

#endif /* RADENGINE_CONTAINER_COMPONENT_HPP_ */
