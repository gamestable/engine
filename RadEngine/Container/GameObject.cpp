/*
 * GameObject.cpp
 *
 *  Created on: Feb 1, 2017
 *      Author: Steffen Kremer
 */

#include "GameObject.hpp"
#include "GameState.hpp"
#include "RadEngine/RadEngine.hpp"

namespace rad
{
namespace engine
{

CGameObject::CGameObject(std::string name, CGameState *state, CGameObject *self, CGameObject *parent, bool heap) : transform(self), m_nameHash(HSID(name.c_str())), m_state(state), m_self(self), m_heap(heap)
{
	m_name = name;
	m_nameHash = HSID(name.c_str());
	m_sceneNode = state->AddObject(self, parent);
	m_index = state->FindObject(this);
	if (parent!=nullptr)
		m_parentObjectIndex = state->FindObject(parent);
	if (heap)
	{
		//Init();
	}
}

CGraphNode<CGameObjectEntry>* CGameObject::GetSceneNode()
{
	return m_sceneNode;
}

CGameObject::~CGameObject()
{
	m_sceneNode->Destroy();
	delete m_sceneNode;
	m_sceneNode = nullptr;
	if (!IsInHeap())
	{
		m_state->RemoveObject(m_self);
	}
}

}
}
