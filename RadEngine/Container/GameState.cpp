/*
 * GameState.cpp
 *
 *  Created on: Feb 1, 2017
 *      Author: Steffen Kremer
 */

#include "GameState.hpp"
#include "RadEngine/RadEngine.hpp"
#include "RadEngine/DataTypes/Index.hpp"
#include <stack>

namespace rad
{
namespace engine
{

CGameState::~CGameState()
{
	RadEngine.Debug.Log("Destroy State...");
	if (m_object.size())
	{
		RadEngine.Debug.Warning("Removing "+std::to_string(m_object.size())+" Objects from "+m_name+" in deconstructor!!! Crash potential!");
		for (unsigned int i=0;i<m_object.size();i++)
		{
			RadEngine.Debug.Log("Removing: "+std::to_string(i)+" - "+std::to_string((unsigned int)m_object.at(i).object)+"/"+std::to_string(m_object.size())+" - "+m_object.at(i).object->GetName()); //);//
			delete m_object.at(i).object;
		}
		RadEngine.Debug.Warning("Removed and safely finished!");
	}
	m_sceneGraph.Destroy();
}

void CGameState::DestroyObjects()
{
	delete m_physicsWorld;
	m_physicsWorld = nullptr;
	for (unsigned int i=0;i<m_object.size();i++)
	{
		RadEngine.Debug.Info("Destroying: "+std::to_string(i)+" - "+std::to_string((unsigned int)m_object.at(i).object)+"/"+std::to_string(m_object.size())+" - "+m_object.at(i).object->GetName()); //);//
		m_object.at(i).object->Destroy();
	}
	std::vector<unsigned int> removeThem;
	for (unsigned int i=0;i<m_object.size();i++)
	{
		if (m_object.at(i).object->IsInHeap())
		{
			//RadEngine.Debug.Info("Removing: "+std::to_string(i)+" - "+std::to_string((unsigned int)m_object.at(i).object)+"/"+std::to_string(m_object.size())+" - "+m_object.at(i).object->GetName()); //);//
			removeThem.insert(removeThem.begin(),m_object.at(i).index);
			delete m_object.at(i).object;
			//RadEngine.Debug.Info("Removed!");
		}
	}
	while(!removeThem.empty())
	{
		RemoveObject(GetObject((unsigned int)removeThem.back()));
		removeThem.pop_back();
	}
	m_object.clear(); //Ohhhhmmmmmmm wtf?
	//Scene Graph
	m_sceneGraph.DestroySubs();
}

bool CGameState::InitObjects()
{
	b2Vec2 gravity(m_physicsGravity.x, m_physicsGravity.y);
	m_physicsWorld = new b2World(gravity);
	std::stack<CGraphNode<CGameObjectEntry>*> stck;
	stck.push(&m_sceneGraph);
	while(!stck.empty())
	{
		CGraphNode<CGameObjectEntry>* nd = stck.top();
		stck.pop();
		if (!nd->IsRoot())
			if (!nd->GetValue().object->Init())
				return false;
		if (nd->CountSubs())
		{
			for (unsigned int i=nd->CountSubs();i>0;--i)
			{
				stck.push(nd->GetSub(i-1));
			}
		}
	}
	return true;
}

bool CGameState::HandleEventObjects()
{
	/*std::stack<CGraphNode<CGameObjectEntry>*> stck;
	stck.push(&m_sceneGraph);
	while(!stck.empty())
	{
		CGraphNode<CGameObjectEntry>* nd = stck.top();
		stck.pop();
		if (!nd->IsRoot())
			nd->GetValue().object->HandleEvent();
		if (nd->CountSubs())
		{
			for (unsigned int i=nd->CountSubs();i>0;--i)
			{
				stck.push(nd->GetSub(i-1));
			}
		}
	}*/

	/*for (unsigned int i=0;i<m_object.size();i++)
	{
		m_object.at(i).object->HandleEvent();
	}*/
	return true;
}

bool CGameState::UpdateObjects()
{
	//m_physicsWorld->Step((RadEngine.Time.ScaledDelta()), m_physicsVelocityIterations, m_physicsPositionIterations);
	m_physicsWorld->Step((1.0f/40.0f)*Time->timeScale, m_physicsVelocityIterations, m_physicsPositionIterations);
	std::stack<CGraphNode<CGameObjectEntry>*> stck;
	stck.push(&m_sceneGraph);
	while(!stck.empty())
	{
		CGraphNode<CGameObjectEntry>* nd = stck.top();
		stck.pop();
		if (!nd->IsRoot())
			nd->GetValue().object->Update();
		if (nd->CountSubs())
		{
			for (unsigned int i=nd->CountSubs();i>0;--i)
			{
				stck.push(nd->GetSub(i-1));
			}
		}
	}
	return true;
}

bool CGameState::DrawObjects()
{
	std::stack<CGraphNode<CGameObjectEntry>*> stck;
	stck.push(&m_sceneGraph);
	while(!stck.empty())
	{
		CGraphNode<CGameObjectEntry>* nd = stck.top();
		stck.pop();
		if (!nd->IsRoot())
			nd->GetValue().object->Draw();
		if (nd->CountSubs())
		{
			for (unsigned int i=nd->CountSubs();i>0;--i)
			{
				stck.push(nd->GetSub(i-1));
			}
		}
	}
	return true;
}

CGraphNode<CGameObjectEntry>* CGameState::AddObject(CGameObject *obj, CGameObject *parentObj)
{
	CGameObjectEntry tmp;
	tmp.object = obj;
	tmp.index = m_FindFreeIndex();
	if (tmp.index==0)
	{
		RadEngine.Debug.Error("Too many GameObjects in "+m_name);
	}
	m_object.insert(m_object.begin(), tmp);
	CGraphNode<CGameObjectEntry>* ret;
	if (parentObj!=nullptr)
	{
		ret = m_sceneGraph.NewSub(obj->GetName(), parentObj->GetSceneNode(), tmp);
	}
	ret =  m_sceneGraph.NewSub(obj->GetName(), &m_sceneGraph, tmp);
	return ret;
	//RadEngine.Debug.Info("Added Object "+obj->GetName());
}

CGameObject* CGameState::GetObject(unsigned int index)
{
	if (index==0||index>m_object.size())
	{
		RadEngine.Debug.Warning("Null Index by Get in "+m_name);
		return nullptr;
	}
	for (unsigned int i=0;i<m_object.size();i++)
	{
		if (m_object.at(i).object!=nullptr)
		{
			if (m_object.at(i).index==index)
			{
				return m_object.at(i).object;
			}
		}
	}
	RadEngine.Debug.Warning("Cant find index "+std::to_string(index)+" by Get in "+m_name);
	return nullptr;
}

unsigned int CGameState::FindObject(CGameObject *obj)
{
	if (obj==nullptr)
	{
		RadEngine.Debug.Warning("Null Pointer by Find in "+m_name);
		return 0;
	}
	for (unsigned int i=0;i<m_object.size();i++)
	{
		if (m_object.at(i).object==obj)
		{
			return m_object.at(i).index;
		}
	}
	RadEngine.Debug.Warning("Cant find pointer "+std::to_string((unsigned int)obj)+" by Find in "+m_name);
	return 0;
}

void CGameState::RemoveObject(CGameObject *obj)
{
	if (obj==nullptr)
	{
		RadEngine.Debug.Warning("Null Pointer by Remove in "+m_name);
		return;
	}
	if (m_object.size()>0)
	{
		bool found = false;
		for (unsigned int i=0;i<m_object.size()&&!found;i++)
		{
			if (m_object.at(i).object==obj)
			{
				//obj->GetSceneNode()->Destroy(); transfer to object destructor?
				m_object.erase(m_object.begin() + i);
				found = true;
			}
		}
		if (!found)
		{
			RadEngine.Debug.Warning("Pointer by Remove in "+m_name+" not found");
		}
	}
	else
	{
		RadEngine.Debug.Warning("No Pointers to Remove in "+m_name);
	}
}

void CGameState::RemoveObject(unsigned int index)
{
	if (index==0||index>m_object.size())
	{
		RadEngine.Debug.Warning("Null Index by RemoveObject in "+m_name);
		return;
	}
	if (m_object.size()>0)
	{
		bool found = false;
		for (unsigned int i=0;i<m_object.size()&&!found;i++)
		{
			if (m_object.at(i).object!=nullptr)
			{
				if (m_object.at(i).index==index)
				{
					m_object.erase(m_object.begin() + i);
				}
			}
		}
		if (!found)
		{
			RadEngine.Debug.Warning("Cant find index "+std::to_string(index)+" by RemoveObject in "+m_name);
		}
	}
	else
	{
		RadEngine.Debug.Warning("No index to Remove in "+m_name);
	}
}

b2World *CGameState::GetPhysicsWorld()
{
	if (m_physicsWorld==nullptr)
	{
		RadEngine.Debug.Warning("Physic World is null in "+m_name);
	}
	return m_physicsWorld;
}

unsigned int CGameState::m_FindFreeIndex()
{
	for (Index curr=1;curr<MAX_INDEX-4;curr++)
	{
		bool found = false;
		for (unsigned int i=0;i<this->m_object.size()&&found==false;i++)
		{
			if (this->m_object.at(i).object!=nullptr)
			{
				if (this->m_object.at(i).index==curr)
				{
					found=true;
				}
			}
		}
		if (found==false)
		{
			return curr;
		}
	}
	RadEngine.Debug.Error("Too many GameObjects in "+m_name);
	RadEngine.Debug.Crash();
	return 0;
}

}
}
