/*
 * Debug.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_DEBUG_DEBUG_CPP_
#define RADENGINE_DEBUG_DEBUG_CPP_

#include "Debug.hpp"
#include "Windows.h"

namespace rad
{
namespace engine
{

void CDebug::Error(std::string str)
{
	Log("ERROR: "+str);
	MessageBoxA(NULL,str.c_str(),"Error",0);
}

}
}

#endif /* RADENGINE_DEBUG_DEBUG_CPP_ */
