/*
 * Debug.hpp
 *
 *  Created on: 31.12.2016
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_DEBUG_DEBUG_HPP_
#define RADENGINE_DEBUG_DEBUG_HPP_

#include <fstream>
#include <iostream>

namespace rad
{
namespace engine
{

class CDebug
{
public:
	CDebug()
	{
		m_logFile.open("radengine.log",std::ios::out|std::ios::trunc);
		m_logFileOpen = m_logFile.is_open();
		if (!m_logFileOpen)
		{
			std::cout << "ERROR: Cant open LogFile radengine.log!" << std::endl;
			std::flush(std::cout);
		}
	}
	~CDebug()
	{
		if (m_logFileOpen)
		{
			m_logFile.flush();
			m_logFile.close();
		}
	}

	void Log(std::string str)
	{
		if (m_logFileOpen)
		{
			m_logFile << str << std::endl;
		}
		std::cout << str << std::endl;
		std::flush(std::cout);
	}

	void Info(std::string str)
	{
		if (m_levelInfo)
			Log("Info: "+str);
	}

	void Warning(std::string str)
	{
		if (m_levelWarning)
			Log("Warning: "+str);
	}

	void Error(std::string str);

	void SetLevel(bool info, bool warning)
	{
		m_levelInfo = info;
		m_levelWarning = warning;
	}

	void Crash()
	{
		Error("!!!SELFCRASH!!!");
		int *crash = (int *)0xFFFFFFFF;
		*crash = 0xFFFFFFFF;
	}

private:
	bool m_logFileOpen = false;
	std::ofstream m_logFile;
	bool m_levelInfo = false;
	bool m_levelWarning = false;
};

}
}

#endif /* RADENGINE_DEBUG_DEBUG_HPP_ */
