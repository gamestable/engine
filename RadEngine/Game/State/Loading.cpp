/*
 * Loading.cpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#include "Loading.hpp"
#include "../../RadEngine.hpp"
#include "SPGame.hpp"

namespace rad
{
namespace engine
{

CLoadingState CLoadingState::m_LoadingState;

bool CLoadingState::Init()
{
	Loader->OpenContainer("./data/textures.bin");
	return true;
}

void CLoadingState::Destroy()
{

}

void CLoadingState::Pause()
{

}

void CLoadingState::Resume()
{

}

void CLoadingState::HandleEvent()
{

}

void CLoadingState::Update()
{
	Loader->LoadResources(Loader->GetContainerCount());
	RadEngine.ChangeState(CSPGameState::Instance());
}

void CLoadingState::Draw()
{

}

}
}
