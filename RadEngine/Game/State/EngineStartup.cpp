/*
 * EngineStartup.cpp
 *
 *  Created on: May 2, 2017
 *      Author: Steffen Kremer
 */

#include "EngineStartup.hpp"
#include "Loading.hpp"
#include "RadEngine/RadEngine.hpp"

namespace rad
{
namespace engine
{

CEngineStartupState CEngineStartupState::m_EngineStartupState;

bool CEngineStartupState::Init()
{
	m_banner.Init(Resource->Texture.Find(HSID("engine_startup")));
	RadEngine.ChangeState(CLoadingState::Instance());
	return true;
}

void CEngineStartupState::Destroy()
{
	m_banner.Destroy();
}

void CEngineStartupState::Pause()
{

}

void CEngineStartupState::Resume()
{

}

void CEngineStartupState::HandleEvent()
{

}

void CEngineStartupState::Update()
{
	m_banner.color.a -= 0.006f;//* Time->Delta();
	if (m_banner.color.a<=0.02f)
	{
		RadEngine.ChangeState(CLoadingState::Instance());
	}
	m_banner.Update();
}

void CEngineStartupState::Draw()
{
	m_banner.Draw();
}

}
}
