/*
 * EngineStartup.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_GAME_STATE_ENGINESTARTUP_HPP_
#define RADENGINE_GAME_STATE_ENGINESTARTUP_HPP_

#include "../../Container/GameState.hpp"

namespace rad
{
namespace engine
{

class CEngineStartupState : public CGameState
{
public:
	bool Init();
	void Destroy();

	void Pause();
	void Resume();

	void HandleEvent();
	void Update();
	void Draw();

	static CEngineStartupState* Instance() {
		return &m_EngineStartupState;
	}

protected:
	CEngineStartupState() : CGameState("EngineStartup") , m_banner(nullptr) {}
	~CEngineStartupState() {}

private:
	static CEngineStartupState m_EngineStartupState;

	CSpriteRenderer m_banner;
};

}
}

#endif /* RADENGINE_GAME_STATE_ENGINESTARTUP_HPP_ */
