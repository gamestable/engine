/*
 * SPGame.hpp
 *
 *  Created on: May 9, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_GAME_STATE_SPGAME_HPP_
#define RADENGINE_GAME_STATE_SPGAME_HPP_

#include "../../Container/GameState.hpp"

namespace rad
{
namespace engine
{

class CSPGameState : public CGameState
{
public:
	bool Init();
	void Destroy();

	void Pause();
	void Resume();

	void HandleEvent();
	void Update();
	void Draw();

	static CSPGameState* Instance() {
		return &m_SPGameState;
	}

protected:
	CSPGameState() : CGameState("SPGameState") {}
	~CSPGameState() {}

private:
	static CSPGameState m_SPGameState;
};

}
}

#endif /* RADENGINE_GAME_STATE_SPGAME_HPP_ */
