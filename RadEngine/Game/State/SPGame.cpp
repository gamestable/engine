/*
 * SPGame.cpp
 *
 *  Created on: May 9, 2017
 *      Author: Steffen Kremer
 */

#include "SPGame.hpp"
#include "../Objects/PLayer.hpp"

namespace rad
{
namespace engine
{

CSPGameState CSPGameState::m_SPGameState;

bool CSPGameState::Init()
{
	this->SetPhysicsGravity(0,0);
	CPlayer *player = new CPlayer(this);
	player->transform.localPosition.Set(1920/2,1080/2);
	player->Init();
	return true;
}

void CSPGameState::Destroy()
{

}

void CSPGameState::Pause()
{

}

void CSPGameState::Resume()
{

}

void CSPGameState::HandleEvent()
{

}

void CSPGameState::Update()
{

}

void CSPGameState::Draw()
{

}

}
}
