/*
 * Loading.hpp
 *
 *  Created on: Jan 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_GAME_STATE_LOADING_HPP_
#define RADENGINE_GAME_STATE_LOADING_HPP_

#include "../../Container/GameState.hpp"

namespace rad
{
namespace engine
{

class CLoadingState : public CGameState
{
public:
	bool Init();
	void Destroy();

	void Pause();
	void Resume();

	void HandleEvent();
	void Update();
	void Draw();

	static CLoadingState* Instance() {
		return &m_LoadingState;
	}

protected:
	CLoadingState() : CGameState("StartupLoading") {}
	~CLoadingState() {}

private:
	static CLoadingState m_LoadingState;
};

}
}

#endif /* RADENGINE_GAME_STATE_LOADING_HPP_ */
