/*
 * Game.hpp
 *
 *  Created on: 31.12.2016
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_GAME_GAME_HPP_
#define RADENGINE_GAME_GAME_HPP_

#include <iostream>

namespace rad
{
namespace engine
{

class CGame
{
public:
	std::string GetTitle() {return m_title;}
	std::string GetVersion() {return std::to_string(m_version[0])+"."+std::to_string(m_version[1])+"."+std::to_string(m_version[2]);}

private:
	std::string m_title = "Techdemo";
	char m_version[3] {0,0,0};
};

extern CGame Game;

}
}

#endif /* RADENGINE_GAME_GAME_HPP_ */
