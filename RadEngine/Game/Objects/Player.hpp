/*
 * CPlayer.hpp
 *
 *  Created on: Apr 24, 2017
 *      Author: Steffen Kremer
 */

#ifndef RADENGINE_GAME_STATE_PLAYER_HPP_
#define RADENGINE_GAME_STATE_PLAYER_HPP_

#include "RadEngine/Datatypes/Index.hpp"
#include "RadEngine/RadEngine.hpp"
#include "RadEngine/Container/Components/IncludeComponents.hpp"

namespace rad
{
namespace engine
{

class CGameState;

class CPlayer : public CGameObject
{
public:
	CPlayer(CGameState* state, CGameObject* parent = nullptr, bool heap = false) : CGameObject("Player", state, this, nullptr, heap), m_sprite(this), m_legAnimation(this), m_statsText(this) {};

	virtual bool Init();
	virtual void Destroy();
	virtual void HandleEvent() {}
	virtual void Update();
	void UpdateComponents();
	virtual void Draw();

private:
	CSpriteRenderer m_sprite;
	CSpriteAnimation m_legAnimation;

	CTextRenderer m_statsText;

	Vector2f m_cursor;
	Vector2f m_cursorSpeed; //Percentage from cursor delta

	float m_moveSpeedForward = 7.0f;
	float m_moveSpeedBackward = 3.0f;
	float m_moveSpeedSideward = 5.0f;

	b2Body *m_physicsBody = nullptr;

	bool m_isLocal = true;
};

}
}
#endif /* RADENGINE_GAME_STATE_PLAYER_HPP_ */
