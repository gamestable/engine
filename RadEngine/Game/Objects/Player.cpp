/*
 * Player.cpp
 *
 *  Created on: May 9, 2017
 *      Author: Steffen Kremer
 */

#include "Player.hpp"

namespace rad
{
namespace engine
{

bool CPlayer::Init()
{
	//Textures
	Resource->Texture.Get(Resource->Texture.Find(HSID("player")))->offset.Set(198,252);
	m_sprite.Init(Resource->Texture.Find(HSID("player")));
	//Cursor
	m_cursor.Set(Input->Mouse.GetAbsX(), Input->Mouse.GetAbsY());
	m_cursorSpeed.Set(0.08f,0.08f);

	CResourceFont *t = new CResourceFont();
	t->LoadFromFile("F:\\Projects\\C++\\RadEngine\\Both\\data\\fonts\\GOODTIME.ttf", 22);

	std::cout << Resource->Font.Find(t) << std::endl;
	m_statsText.Init(Resource->Font.Find(t),22);
	m_statsText.color = ColorF::Green;
	m_statsText.text = "Hi! . M";

	UpdateComponents();

	//Player physics body
	b2BodyDef bodyDef;
	bodyDef.position.Set(transform.GetGlobalPosition().x/100.f, transform.GetGlobalPosition().y/100.f);
	bodyDef.type = b2_dynamicBody;
	bodyDef.linearDamping = 4.0f;
	bodyDef.angularDamping = 0.01f;
	bodyDef.allowSleep = false;
	bodyDef.awake = true;
	bodyDef.active = true;
	bodyDef.gravityScale = 1.0f;
	m_physicsBody = GetState()->GetPhysicsWorld()->CreateBody(&bodyDef);

	b2CircleShape circle;
	circle.m_radius = 0.9f;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	//fixtureDef.density = 1.0f;
	//fixtureDef.friction = 0.3f;

	m_physicsBody->CreateFixture(&fixtureDef);
	return true;
}

void CPlayer::Destroy()
{
	m_sprite.Destroy();
	m_statsText.Destroy();
}

void CPlayer::Update()
{
	//Cursor
	float dX = Input->Mouse.GetAbsX()-m_cursor.x;
	float dY = Input->Mouse.GetAbsY()-m_cursor.y;
	if (dX>1.0f||dX<-1.0f) { m_cursor.x += dX * m_cursorSpeed.x; }
	else { m_cursor.x = Input->Mouse.GetAbsX(); }
	if (dY>1.0f||dY<-1.0f) { m_cursor.y += dY * m_cursorSpeed.y; }
	else { m_cursor.y = Input->Mouse.GetAbsY(); }
	//Turn to calculated cursor
	transform.LookAt(Vector2f(m_cursor.x,m_cursor.y),-180);

	//Physics
	transform.localPosition.x = m_physicsBody->GetPosition().x * 100.f;
	transform.localPosition.y = m_physicsBody->GetPosition().y * 100.f;

	float sprintMultiplier = 1.f;
	if (Input->Keyboard.Down(KEYBOARD_E))
	{
		sprintMultiplier = 2.5f;
	}
	if (Input->Keyboard.Down(KEYBOARD_W))
	{
		m_physicsBody->ApplyForceToCenter(b2Vec2(0, -m_moveSpeedForward*sprintMultiplier), true);
	}
	else if (Input->Keyboard.Down(KEYBOARD_S))
	{
		m_physicsBody->ApplyForceToCenter(b2Vec2(0, m_moveSpeedForward*sprintMultiplier), true);
	}
	if (Input->Keyboard.Down(KEYBOARD_A))
	{
		m_physicsBody->ApplyForceToCenter(b2Vec2(-m_moveSpeedForward*sprintMultiplier, 0), true);
	}
	else if (Input->Keyboard.Down(KEYBOARD_D))
	{
		m_physicsBody->ApplyForceToCenter(b2Vec2(m_moveSpeedForward*sprintMultiplier, 0), true);
	}

	UpdateComponents();
}

void CPlayer::UpdateComponents()
{
	transform.Update();
	m_sprite.Update();
	m_statsText.Update();
}

void CPlayer::Draw()
{
	m_sprite.Draw();
	m_statsText.Draw();
}

}
}
