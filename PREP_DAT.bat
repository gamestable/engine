@echo off
echo "Working in:"
echo %cd%
echo Deleting old containers...
del /F /Q "..\Both\data\fonts.bin"
del /F /Q "..\Both\data\shaders.bin"
del /F /Q "..\Both\data\sounds.bin"
del /F /Q "..\Both\data\textures.bin"
del /F /Q "..\Both\data\base.bin"
echo Creating new containers...
..\..\RadTools\Debug\RadTools.exe resource \A \C \F "..\Both\data\fonts" "..\Both\data\fonts.bin"
..\..\RadTools\Debug\RadTools.exe resource \A \C \F "..\Both\data\shaders" "..\Both\data\shaders.bin"
..\..\RadTools\Debug\RadTools.exe resource \A \C \F "..\Both\data\sounds" "..\Both\data\sounds.bin"
..\..\RadTools\Debug\RadTools.exe resource \A \C \F "..\Both\data\textures" "..\Both\data\textures.bin"
..\..\RadTools\Debug\RadTools.exe resource \A \C \F "..\Both\data\base" "..\Both\data\base.bin"
echo ---------
echo Finished!
PAUSE